<?php
/**
 *
 * @category    Scommerce
 * @package     Scommerce_UniversalAnalytics
 * @author		Scommerce Mage (core@scommerce-mage.co.uk)
 */
class Scommerce_UniversalAnalytics_Block_List extends Mage_Core_Block_Template
{
    public function getProductCollection()
    {
       return $this->getLayout()->getBlockSingleton('catalog/product_list')->getLoadedProductCollection();
    }
}