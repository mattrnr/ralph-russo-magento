<?php
/**
 *
 * @category    Scommerce
 * @package     Scommerce_UniversalAnalytics
 * @author		Scommerce Mage (core@scommerce-mage.co.uk)
 */
class Scommerce_UniversalAnalytics_Block_New extends Mage_Catalog_Block_Product_New
{
	
	protected function _construct()
    {
        parent::_construct();
		$this->setTemplate('scommerce/universalanalytics/new.phtml');
	}
}