<?php
$installer = $this;

$installer->startSetup();

$sales_setup =  new Mage_Sales_Model_Mysql4_Setup('sales_setup');

$attribute  = array(
	'type'          => 'int',
	'label'         => 'Sent Data To Google',
	'default'       => '',
	'visible'       => false,
	'required'      => false,
	'user_defined'  => true,
	'searchable'    => false,
	'filterable'    => false,
	'comparable'    => false );

$installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_order'),
        'sent_data_to_google',
        'int'
    );
	
$sales_setup->addAttribute('order', 'sent_data_to_google', $attribute);

$installer->endSetup();