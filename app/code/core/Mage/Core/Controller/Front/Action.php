<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Base front controller
 *
 * @category   Mage
 * @package    Mage_Core
 */
class Mage_Core_Controller_Front_Action extends Mage_Core_Controller_Varien_Action
{
    /**
     * Session namespace to refer in other places
     */
    const SESSION_NAMESPACE = 'frontend';

    /**
     * Add secret key to url config path
     */
    const XML_CSRF_USE_FLAG_CONFIG_PATH   = 'system/csrf/use_form_key';

    /**
     * Currently used area
     *
     * @var string
     */
    protected $_currentArea = 'frontend';

    /**
     * Namespace for session.
     *
     * @var string
     */
    protected $_sessionNamespace = self::SESSION_NAMESPACE;

    /**
     * Predispatch: should set layout area
     *
     * @return Mage_Core_Controller_Front_Action
     */
    public function preDispatch()
    {
        $this->getLayout()->setArea($this->_currentArea);

        parent::preDispatch();
        return $this;
    }

    /**
     * Postdispatch: should set last visited url
     *
     * @return Mage_Core_Controller_Front_Action
     */
    public function postDispatch()
    {
        parent::postDispatch();
        if (!$this->getFlag('', self::FLAG_NO_START_SESSION )) {
            Mage::getSingleton('core/session')->setLastUrl(Mage::getUrl('*/*/*', array('_current'=>true)));
        }
        return $this;
    }

    /**
     * Translate a phrase
     *
     * @return string
     */
    public function __()
    {
        $args = func_get_args();
        $expr = new Mage_Core_Model_Translate_Expr(array_shift($args), $this->_getRealModuleName());
        array_unshift($args, $expr);
        return Mage::app()->getTranslator()->translate($args);
    }

    public function zendeskPost($post, $requestFrom) {
        $enquiryType = trim($post['lookbook-runway-type']);
        $subjectLine = "Ralph & Russo Enquiry";
        $type = array("haute_couture_enquiry"=>"Couture","shoes_enquiry"=>"Shoes","handbags_enquiry"=>"Handbags","press_enquiry"=>"Press","business_opportunities"=>"Business Opportuntities","career_enquiry"=>"Career Enquiry","general_enquiry"=>"General","ready_to_wear_enquiry"=>"Ready To Wear","ready_to_wear"=>"Ready To Wear");
        
        if ($requestFrom == "LookBook Popup") {
            if ($enquiryType == "ready_to_wear") {
                $subjectLine = "Ralph & Russo Lookbook Enquiry - Ready To Wear";
                $enquiryType = "ready_to_wear_enquiry";
            } else if ($enquiryType == "haute_couture_enquiry") {
                $subjectLine = "Ralph & Russo Lookbook Enquiry - Couture";
            }
        } else if ($requestFrom == "Appiontments Popup") {
            $enquiryType = "appiontments";
            $subjectLine = "Ralph & Russo Appiontment Request - " . ucwords(str_replace("-", " ", str_replace("request_appointment_", "", trim($post['request_appointment']))));
        } else if ($requestFrom == "Contact Us") {
            $subjectLine = "Ralph & Russo Enquiry - " . $type[$post["enquiry-type"]];
        }

        $timezone = array("-12"=>"","-11"=>"");

                $zendesk_ticket_post_body = array(
                    "request" => array(
                        "requester" => array(
                            "name" => trim($post['first-name']) . " " . trim($post['last-name']),
                            "email" => trim($post['email'])
                        ),
                        "subject" => $subjectLine,
                        "comment" => array(
                            "body" => trim($post['message'])
                        ),
                        "via" => array(
                            "channel" => "Dev Magento Zendesk API"
                        ),
                        "custom_fields" => array(
                        )
                    )
                );

                if ($requestFrom == "LookBook Popup") {
                    $customFields1 = array("id" => "360001047117","value" => "contactus:" . $enquiryType);
                    $customFields2 = array("id" => "360001038377","value" => trim($post['country']));
                    $customFields3 = array("id" => "360001172458","value" => trim($post["lookbook-season"]));
                    $customFields4 = array("id" => "360001172478","value" => trim($post["lookbook-look"]));
                    $customFields5 = array("id" => "360001172498","value" => trim($post["preferred_contact_method"]));
                    $customFields6 = array("id" => "360001345777","value" => "enquiry_source_lookbook_form");
                    array_push($zendesk_ticket_post_body["request"]["custom_fields"], $customFields1, $customFields2, $customFields3, $customFields4, $customFields5, $customFields6);
                } else if ($requestFrom == "Appiontments Popup") {
                    $customFields1 = array("id" => "360001047117","value" => "contactus:" . $enquiryType);
                    $customFields2 = array("id" => "360001038377","value" => trim($post['country']));
                    $customFields3 = array("id" => "360001172498","value" => trim($post["preferred_contact_method"]));
                    $customFields4 = array("id" => "360000964457","value" => trim($post["request_appointment"]));
                    $customFields5 = array("id" => "360001345777","value" => "enquiry_source_appointments_form");
                    array_push($zendesk_ticket_post_body["request"]["custom_fields"], $customFields1, $customFields2, $customFields3, $customFields4, $customFields5);
                } else if ($requestFrom == "Contact Us") {
                    $customFields1 = array("id" => "360001047117","value" => "contactus:" . $post["enquiry-type"]);
                    $customFields2 = array("id" => "360001038377","value" => trim($post['country']));
                    $customFields3 = array("id" => "360001172498","value" => trim($post["preferred_contact_method"]));
                    $customFields4 = array("id" => "360001345777","value" => "enquiry_source_contact_us_form");
                    array_push($zendesk_ticket_post_body["request"]["custom_fields"], $customFields1, $customFields2, $customFields3, $customFields4);
                }
                
                $zendesk_user_post_body = array(
                    "user" => array(
                        "name" => trim($post['first-name']) . " " . trim($post['last-name']),
                        "email" => trim($post['email']),
                        "phone" => trim($post['phone']),
                        "locale" => "en-GB",
                        // "time_zone" => trim($post['geolocation']),
                        "time_zone" => "London",
                        "tags" => array(
                            $type[trim($post['enquiry-type'])]
                        ),
                        "user_fields" => array(
                            "user_title" => trim($post['title']),
                            "user_firstname" => trim($post['first-name']),
                            "user_secondname" => trim($post['last-name']),
                            "user_geolocation" => trim($post['country']),
                            "preferred_contact_method" => trim($post["preferred_contact_method"])
                        )
                    )
                );
                
                $curl = curl_init();
                
                curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://ralphandrussoclientcare.zendesk.com/api/v2/users/create_or_update.json",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_SSLVERSION => 6,
                  CURLOPT_CUSTOMREQUEST => "POST",
                  CURLOPT_POSTFIELDS => json_encode($zendesk_user_post_body),
                  CURLOPT_COOKIE => "__cfduid=d2945b5c3f4a08da25ccc3239f1e4b2781552574898; __cfruid=8116ec90090bfbd5c50f3a4b177c9789de96efc1-1553185246; _zendesk_session=BAh7CEkiD3Nlc3Npb25faWQGOgZFVEkiJWM5Njk5MmM3NzQ0NWIyYTJkMTZlOTkzM2ZhYmMxMzMyBjsAVEkiDGFjY291bnQGOwBGaQMhJYxJIgpyb3V0ZQY7AEZpAzXLKA%253D%253D--2413a275d65eae5cdbd9a2b4e79792335be93a2a",
                  CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cGhpbGxpcC5tYXh3ZWxsQHJhbHBoYW5kcnVzc28uY29tOkZhc2hpb24xMjM0",
                    "content-type: application/json"
                  ),
                ));
                
                curl_exec($curl);
                $err = curl_error($curl);
            
                if ($err) {
                    curl_close($curl);
                    return "error 1 " . $err;
                } else {
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => "https://ralphandrussoclientcare.zendesk.com/api/v2/requests.json",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_SSLVERSION => 6,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => json_encode($zendesk_ticket_post_body),
                        CURLOPT_COOKIE => "__cfduid=d2945b5c3f4a08da25ccc3239f1e4b2781552574898; __cfruid=8116ec90090bfbd5c50f3a4b177c9789de96efc1-1553185246; _zendesk_session=BAh7CEkiD3Nlc3Npb25faWQGOgZFVEkiJWM5Njk5MmM3NzQ0NWIyYTJkMTZlOTkzM2ZhYmMxMzMyBjsAVEkiDGFjY291bnQGOwBGaQMhJYxJIgpyb3V0ZQY7AEZpAzXLKA%253D%253D--2413a275d65eae5cdbd9a2b4e79792335be93a2a",
                        CURLOPT_HTTPHEADER => array(
                          "authorization: Basic cGhpbGxpcC5tYXh3ZWxsQHJhbHBoYW5kcnVzc28uY29tOkZhc2hpb24xMjM0",
                          "content-type: application/json"
                        ),
                    ));

                    curl_exec($curl);
                    $err2 = curl_error($curl);
                
                    if ($err2) {
                        curl_close($curl);
                        return "error 2 " . $err;
                    } else {
                        curl_close($curl);
                        return;
                    }
                }
    }

    /**
     * Declare headers and content file in response for file download
     *
     * @param string $fileName
     * @param string|array $content set to null to avoid starting output, $contentLength should be set explicitly in
     *                              that case
     * @param string $contentType
     * @param int $contentLength    explicit content length, if strlen($content) isn't applicable
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _prepareDownloadResponse($fileName, $content, $contentType = 'application/octet-stream',
        $contentLength = null
    ) {
        $session = Mage::getSingleton('admin/session');
        if ($session->isFirstPageAfterLogin()) {
            $this->_redirect($session->getUser()->getStartupPageUrl());
            return $this;
        }

        $isFile = false;
        $file   = null;
        if (is_array($content)) {
            if (!isset($content['type']) || !isset($content['value'])) {
                return $this;
            }
            if ($content['type'] == 'filename') {
                $isFile         = true;
                $file           = $content['value'];
                $contentLength  = filesize($file);
            }
        }

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true)
            ->setHeader('Content-Length', is_null($contentLength) ? strlen($content) : $contentLength)
            ->setHeader('Content-Disposition', 'attachment; filename="'.$fileName.'"')
            ->setHeader('Last-Modified', date('r'));

        if (!is_null($content)) {
            if ($isFile) {
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();

                $ioAdapter = new Varien_Io_File();
                if (!$ioAdapter->fileExists($file)) {
                    Mage::throwException(Mage::helper('core')->__('File not found'));
                }
                $ioAdapter->open(array('path' => $ioAdapter->dirname($file)));
                $ioAdapter->streamOpen($file, 'r');
                while ($buffer = $ioAdapter->streamRead()) {
                    print $buffer;
                }
                $ioAdapter->streamClose();
                if (!empty($content['rm'])) {
                    $ioAdapter->rm($file);
                }

                exit(0);
            } else {
                $this->getResponse()->setBody($content);
            }
        }
        return $this;
    }

    /**
     * Validate Form Key
     *
     * @return bool
     */
    protected function _validateFormKey()
    {
        $validated = true;
        if (Mage::getStoreConfigFlag(self::XML_CSRF_USE_FLAG_CONFIG_PATH)) {
            $validated = parent::_validateFormKey();
        }
        return $validated;
    }
}
