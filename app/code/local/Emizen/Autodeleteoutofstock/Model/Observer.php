<?php
class Emizen_Autodeleteoutofstock_Model_Observer
{

	protected $_checkedQuoteItems = array();

	public function autodelete(Varien_Event_Observer $observer)
	{

		if(!Mage::getStoreConfig('emizen/emizen/general')) // if not enable extension return false
			return;

		$session = Mage::getSingleton("checkout/session");
		$quote = $session->getQuote();

		$this->checkItemsToDelete($session, $quote);
		$this->checkItemsToUpdateQty($session, $quote);

	}

	private function checkItemsToDelete($session, $quote) {
		$cartItems = $quote->getAllItems();
		$countItemsOutOfStock = 0;

		$sessionMessages = array();

		foreach ($cartItems as $item)
		{
			$productId = $item->getProductId();
			$product = Mage::getModel('catalog/product')->load($productId);
			$stockItem = $product->getStockItem();

			if(!$stockItem->getIsInStock())
				$countItemsOutOfStock++;
		}

		if($countItemsOutOfStock > 0) {
			array_push($sessionMessages, '<h3>WELCOME BACK TO YOUR RALPH & RUSSO SHOPPING BAG</h3>');
			array_push($sessionMessages, '<p>Unfortunately, due to popular demand the following ' . (($countItemsOutOfStock > 1) ? 'items are' : 'item is') . ' no longer in stock:</p>');
		}
		foreach ($cartItems as $item)
		{

			$productId = $item->getProductId();
			$product = Mage::getModel('catalog/product')->load($productId);
			$stockItem = $product->getStockItem();
			if(!$stockItem->getIsInStock())
			{

				// START
				$simpleProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getProduct()->getSku());
				$correctConfigurableProduct = $simpleProduct->getCorrectConfigurableProductForSimpleProduct();
				$variationName = $correctConfigurableProduct->getProductVariation($correctConfigurableProduct, 'Default Selected Variation');

				$productName = $correctConfigurableProduct->getName();
				$categoryUrl = null;
				$categories = $correctConfigurableProduct->getCategoryIds();
				if(!empty($categories)) {
					$category = Mage::getModel('catalog/category')->load($categories[count($categories) - 1]);
					if(!empty($category))
					$categoryUrl = $category->getUrlPath();
				}
				// END

				$produclink = Mage::helper('checkout')->__('<div class="link-wrapper"><a href="%s">%s %s</a> - to discover other variations of this item, please <a href="/%s">click here</a></div>', $correctConfigurableProduct->getProductUrl(), $productName, $variationName, $categoryUrl);
				array_push($sessionMessages, $produclink);

				Mage::helper('checkout/cart')->getCart()->removeItem($item->getId())->save();
				$quote->setHasError(false);

				Mage::getSingleton('checkout/session')->setCartWasUpdated(true);
			}
		}

		if($countItemsOutOfStock > 0) {
			array_push($sessionMessages, '<p>Alternatively, please contact us to seek availability.</p>');
			array_push($sessionMessages, '<div class="btn-wrapper"><a href="/contact-us" class="btn inverted">CONTACT US</a></div>');
		}

		if(!empty($sessionMessages)) {
			$session->addError(implode('', $sessionMessages));
		}
	}

	private function checkItemsToUpdateQty($session, $quote) {

		$cartItems = $quote->getAllItems();

		$sessionMessages = array();

		$updatedQtyItemsCount = 0;

		$updatedQtyItemsMessages = array();

		foreach ($cartItems as $quoteItem) {
			$qty = $quoteItem->getQty();
			$stockItem = $quoteItem->getProduct()->getStockItem();
			$parentStockItem = false;
	        if ($quoteItem->getParentItem()) {
	            $parentStockItem = $quoteItem->getParentItem()->getProduct()->getStockItem();
	        }

			$options = $quoteItem->getQtyOptions();
			if ($options && $qty > 0) {
				$qty = $quoteItem->getProduct()->getTypeInstance(true)->prepareQuoteItemQty($qty, $quoteItem->getProduct());
				$quoteItem->setData('qty', $qty);
			}

			foreach ($options as $option) {
                $optionValue = $option->getValue();
                /* @var $option Mage_Sales_Model_Quote_Item_Option */
                $optionQty = $qty * $optionValue;
                $increaseOptionQty = ($quoteItem->getQtyToAdd() ? $quoteItem->getQtyToAdd() : $qty) * $optionValue;

                $stockItem = $option->getProduct()->getStockItem();

                if ($quoteItem->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                    $stockItem->setProductName($quoteItem->getName());
                }

                /* @var $stockItem Mage_CatalogInventory_Model_Stock_Item */
                if (!$stockItem instanceof Mage_CatalogInventory_Model_Stock_Item) {
                    Mage::throwException(
                        Mage::helper('cataloginventory')->__('The stock item for Product in option is not valid.')
                    );
                }

                /**
                 * define that stock item is child for composite product
                 */
                $stockItem->setIsChildItem(true);
                /**
                 * don't check qty increments value for option product
                 */
                $stockItem->setSuppressCheckQtyIncrements(true);

                $qtyForCheck = $this->_getQuoteItemQtyForCheck(
                    $option->getProduct()->getId(),
                    $quoteItem->getId(),
                    $increaseOptionQty
                );

                $result = $stockItem->checkQuoteItemQty($optionQty, $qtyForCheck, $optionValue);

				if ($result->getHasError() && $result->getQuoteMessageIndex() == 'qty') {
					if (!$stockItem->checkQty($qtyForCheck) || !$stockItem->checkQty($optionQty)) {
						$maxQtyInStock = (int) $stockItem->getQty();

						$quoteItem->setData('qty', $maxQtyInStock);

    					Mage::dispatchEvent('sales_quote_item_qty_set_after', array('item' => $quoteItem));

						$simpleProduct = Mage::getModel('catalog/product')->loadByAttribute('sku', $quoteItem->getProduct()->getSku());
						$correctConfigurableProduct = $simpleProduct->getCorrectConfigurableProductForSimpleProduct();
						$variationName = $correctConfigurableProduct->getProductVariation($correctConfigurableProduct, 'Default Selected Variation');

						$productName = $correctConfigurableProduct->getName();
						$categoryUrl = null;
						$categories = $correctConfigurableProduct->getCategoryIds();
						if(!empty($categories)) {
							$category = Mage::getModel('catalog/category')->load($categories[count($categories) - 1]);
							if(!empty($category))
							$categoryUrl = $category->getUrlPath();
						}
						// END

						$produclink = Mage::helper('checkout')->__('<div class="link-wrapper"><a href="%s">%s %s</a> - to discover other variations of this item, please <a href="/%s">click here</a></div>', $correctConfigurableProduct->getProductUrl(), $productName, $variationName, $categoryUrl);

						array_push($updatedQtyItemsMessages, $produclink);

						$updatedQtyItemsCount++;

					}
				}

				$stockItem->unsIsChildItem();
            }
		}

		if($updatedQtyItemsCount > 0) {
			array_push($sessionMessages, '<h3>WELCOME BACK TO YOUR RALPH & RUSSO SHOPPING BAG</h3>');
			array_push($sessionMessages, '<p>Unfortunately, due to popular demand, the quantity you requested of the following ' . (($updatedQtyItemsCount > 1) ? 'items are' : 'item is') . ' not available:</p>');

			foreach($updatedQtyItemsMessages as $itemMessage)
				array_push($sessionMessages, $itemMessage);

			array_push($sessionMessages, '<p>Alternatively, please contact us to seek availability.</p>');
			array_push($sessionMessages, '<div class="btn-wrapper"><a href="/contact-us" class="btn inverted">CONTACT US</a></div>');
		}

		if(!empty($sessionMessages)) {
			$session->addError(implode('', $sessionMessages));
		}
	}

	protected function _getQuoteItemQtyForCheck($productId, $quoteItemId, $itemQty)
    {
        $qty = $itemQty;
        if (isset($this->_checkedQuoteItems[$productId]['qty']) &&
            !in_array($quoteItemId, $this->_checkedQuoteItems[$productId]['items'])) {
                $qty += $this->_checkedQuoteItems[$productId]['qty'];
        }

        $this->_checkedQuoteItems[$productId]['qty'] = $qty;
        $this->_checkedQuoteItems[$productId]['items'][] = $quoteItemId;

        return $qty;
    }

}
