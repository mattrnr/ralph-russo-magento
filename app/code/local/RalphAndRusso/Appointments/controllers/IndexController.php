<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class RalphAndRusso_Appointments_IndexController extends Mage_Core_Controller_Front_Action
{
    const XML_PATH_EMAIL_SENDER     = 'appointments/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'appointments/email/email_template';
    const XML_PATH_EMAIL_CUSTOMER_CONFIRMATION_TEMPLATE   = 'appointments/email/email_customer_confirmation_template';
    const XML_PATH_ENQUIRY_EMAIL_TEMPLATE   = 'appointments/email/email_enquiry_template';
    const XML_PATH_ENQUIRY_EMAIL_CUSTOMER_CONFIRMATION_TEMPLATE   = 'appointments/email/email_enquiry_customer_confirmation_template';
    const XML_PATH_BETHEFIRSTTOHEAR_EMAIL_TEMPLATE   = 'appointments/email/email_bethefirsttohear_template';
    const XML_PATH_BETHEFIRSTTOHEAR_EMAIL_CUSTOMER_CONFIRMATION_TEMPLATE   = 'appointments/email/email_bethefirsttohear_customer_confirmation_template';
    const XML_PATH_ENABLED          = 'appointments/appointments/enabled';

    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::getStoreConfigFlag(self::XML_PATH_ENABLED)) {
            $this->norouteAction();
        }
    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['title']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['first-name']), 'NotEmpty')) {
                    $error = true;
                    $firstname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['first-name']))) {
                        $firstname = false;
                    } else {
                        $firstname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['last-name']), 'NotEmpty')) {
                    $error = true;
                    $lastname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['last-name']))) {
                        $lastname = false;
                    } else {
                        $lastname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['country']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    echo json_encode(array('response' => 'ERROR'));
                    return;
                }

                if ($firstname == false && $lastname == false) {
                    if (trim($post['first-name']) != trim($post['last-name'])) {
                        $zendesk = $this->zendeskPost($post, "Appiontments Popup");
                        if (!$zendesk) {
                            echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>THANK YOU</h3><p>Thank you for requesting an appointment with Ralph & Russo.
                            A member of our client relations team will respond to your request.<br><br><span style="font-size:10px; font-style: italic;">*Based on uk holiday hours</span></p>'));
                        } else {
                            echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.<br><span style="font-size: 9pt">' . $zendesk . '</span></h3>'));
                        }
                    }  else {
                        echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.</h3>'));
                    }
                } else {
                  echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.</h3>'));
                }
                return;
            } catch (Exception $e) {
                echo json_encode(array('response' => 'ERROR'));
                return;
            }
        }
    }

    public function postenquiryAction()
    {

        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['title']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['first-name']), 'NotEmpty')) {
                    $error = true;
                    $firstname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['first-name']))) {
                        $firstname = false;
                    } else {
                        $firstname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['last-name']), 'NotEmpty')) {
                    $error = true;$lastname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['last-name']))) {
                        $lastname = false;
                    } else {
                        $lastname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['country']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    echo json_encode(array('response' => 'ERROR'));
                    return;
                }
                
                if ($firstname == false && $lastname == false) {
                    if (trim($post['first-name']) != trim($post['last-name'])) {
                        $zendesk =  $this->zendeskPost($post, "LookBook Popup");

                        if (!$zendesk) {
                            echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Thank you for contacting Ralph & Russo</h3><p>A member of our team will respond to your enquiry shortly.<br><br><span style="font-size:10px; font-style: italic;">*Based on uk holiday hours</span></p>'));
                        } else {
                            echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.<br><span style="font-size: 9pt">' . $zendesk . '</span></h3>'));
                        }
                    } else {
                        echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.</h3>'));
                    }
                } else {
                  echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.</h3>'));
                }
                return;
            } catch (Exception $e) {
                echo json_encode(array('response' => 'ERROR'));
                return;
            }
        }
    }

    public function postbethefirsttohearAction()
    {
        $post = $this->getRequest()->getPost();
        if ($post) {
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['title']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['first-name']), 'NotEmpty')) {
                    $error = true;
                    $firstname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['first-name']))) {
                        $firstname = false;
                    } else {
                        $firstname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['last-name']), 'NotEmpty')) {
                    $error = true;
                    $lastname = false;
                } else {
                    if (preg_match("/^[a-zA-Z]+$/", trim($post['last-name']))) {
                        $lastname = false;
                    } else {
                        $lastname = true;
                    }
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['country']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                    $error = true;
                }

                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['category-id']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    echo json_encode(array('response' => 'ERROR'));
                    return;
                }

                $_countries = Mage::getSingleton('objectcache/countrycollection')->get();

                foreach ($_countries as $_country) {
                    if ($_country['value'] != $postObject->country) {
                        continue;
                    }
                    $postObject->country = $_country['label'];
                }

                $mailTemplate = Mage::getModel('core/email_template');

                $category = Mage::getModel('catalog/category')->load($post['category-id']);
                $notificationEmails = $category->getData('notification_emails');

                if ($notificationEmails ==  null) {
                    throw new Exception('Notification emails not set');
                }

                $environmentHelper = Mage::helper('environment');
                if ($environmentHelper->getIsProductionEnvironment()) {
                    $notificationEmailAddresses = explode("|", $notificationEmails);
                    $beTheFirstToHearEmails = array('names' => array(), 'emails' => array());

                    foreach ($notificationEmailAddresses as $notificationEmailAddress) {
                        array_push($beTheFirstToHearEmails['names'], 'Be the First to Hear');
                        array_push($beTheFirstToHearEmails['emails'], $notificationEmailAddress);
                    }
                } else {
                    $beTheFirstToHearEmails = array('names' => array('Be the First to Hear'), 'emails' => array('matthew.hammond@ralphandrusso.com'));
                }

                if ($firstname == false && $lastname == false) {
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplate->setDesignConfig(array('area' => 'frontend'))
                ->setReplyTo($post['email'])
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_BETHEFIRSTTOHEAR_EMAIL_TEMPLATE),
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                    $beTheFirstToHearEmails['emails'],
                    $beTheFirstToHearEmails['names'],
                    array('data' => $postObject)
                    );

                if (!$mailTemplate->getSentSuccess()) {
                    throw new Exception();
                }

                $mailTemplateCustomerConfirmation = Mage::getModel('core/email_template');
                /* @var $mailTemplate Mage_Core_Model_Email_Template */
                $mailTemplateCustomerConfirmation->setDesignConfig(array('area' => 'frontend'))
                ->sendTransactional(
                    Mage::getStoreConfig(self::XML_PATH_BETHEFIRSTTOHEAR_EMAIL_CUSTOMER_CONFIRMATION_TEMPLATE),
                    Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER),
                    $post['email'],
                    $post['first-name'] . ' ' . $post['last-name'],
                    array('data' => $postObject)
                    );

                if (!$mailTemplateCustomerConfirmation->getSentSuccess()) {
                    throw new Exception();
                }

                echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>THANK YOU</h3><p>Thank you for showing interest in this product, you will now receive updates about this piece.<br><br><span style="font-size:10px; font-style: italic;">*Based on uk holiday hours</span></p>'));
              } else {
                echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>Unable to submit your request.</h3>'));
              }
                return;
            } catch (Exception $e) {
                echo json_encode(array('response' => 'ERROR'));
                return;
            }
        }
    }
}
