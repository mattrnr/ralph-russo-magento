<?php
/**
 * Cartalert module observer
 *
 * @author Adjustware
 */
class RalphAndRusso_Cartalert_Model_Cartalert extends AdjustWare_Cartalert_Model_Cartalert
{
    /**
     * @param Mage_Core_Model_Store $store | null
     */
    public function preprocess($store=null){
        if ($this->getIsPreprocessed())
            return $this;
        $this->setIsPreprocessed(1);
        if (!strpos($this->getProducts(),'##'))
            return $this; // new or custom

        if (!$store)
            $store = Mage::app()->getStore($this->getStoreId());

        $baseUrl = $store->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $visibility = Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds();
        $status = Mage::getSingleton('catalog/product_status')->getVisibleStatusIds();
        $items = array();
        $prod = explode('##,', substr($this->getProducts(), 0, -2));
        $mediaPath = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath();
        for ($i=0, $n=sizeof($prod); $i<$n; $i+=2){
            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getStoreId())
                ->load($prod[$i]);

            $product = $product->getCorrectConfigurableProductForSimpleProduct();

            if(in_array($product->getStatus(),$status)&&in_array($product->getVisibility(),$visibility)&&$product->isSaleable())
            {
                $url = $product->getProductUrl();
                $name = $product->getSku();
                $imageTag = '';$hasImage = $product->getData('small_image');
                if((isset($hasImage))&&($hasImage!= 'no_selection') && file_exists($mediaPath.$hasImage))
                {
                    $imageTag ='<img style="vertical-align: middle;max-width:100px; max-height: 150px;display: table-cell;" src="'.Mage::helper('catalog/image')->init($product ,
                    'small_image')->resize(75).'" border="0" />';
                }
                $items[$prod[$i]] = '<a href="'.$url.'" style="display:table;color: #a87357;text-decoration:none;font-weight:bold;">'.$imageTag.'<span style="padding-left:10px;display: table-cell;vertical-align: middle;">'.$name.'</span></a>'; //to omit duplicates
            }
        }

        $this->setProducts(join("<br />\n", $items));
        $this->setIsPreprocessed(1);
        return $this;
    }

}
