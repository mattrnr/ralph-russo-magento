<?php
class RalphAndRusso_Cartalert_Model_Mysql4_Cartalert_Cart extends AdjustWare_Cartalert_Model_Mysql4_Cartalert_Cart
{
    /**
     * @param  array $ids
     * @return array
     */
    protected function _getAbandonedQuotesContent($ids){
        $db = $this->_getReadAdapter();
        $fields = $this->_getRequiredFields();

        $this->_select = $db->select()
            ->from(array('q' => $this->getTable('sales/quote')), $fields)
            ->joinInner(array('i' => $this->getTable('sales/quote_item')), 'q.entity_id=i.quote_id', array())
            ->joinLeft(array('ba' => $this->getTable('sales/quote_address')), 'q.entity_id=ba.quote_id AND ba.address_type="billing"', array())
            ->where('q.entity_id IN(?)', $ids)
            ->where('IFNULL(q.customer_email, ba.email) IS NOT NULL')
            ->where('i.parent_item_id IS NOT NULL')
            ->group('q.entity_id')
            ->limit(50); // we expect that there will be 10-20 carts maximum, because cron runs each hour
        $this->_addFilter('visibility', Mage::getSingleton('catalog/product_visibility')->getVisibleInSiteIds());
        $this->_addFilter('status', Mage::getSingleton('catalog/product_status')->getVisibleStatusIds());

        return $db->fetchAll($this->_select);
    }

    protected function _getTimeout()
    {
        return intVal(Mage::getStoreConfig(self::XML_PATH_TIMEOUT));
    }
}
