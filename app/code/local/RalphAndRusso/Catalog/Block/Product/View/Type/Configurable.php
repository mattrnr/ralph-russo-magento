<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


/**
 * Catalog super product configurable part block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class RalphAndRusso_Catalog_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{
    /**
     * Get Allowed Products
     *
     * @return array
     */
    public function getAllowProducts()
    {
        //Try get from singleton cache manager
        $cacheManager = Mage::getSingleton('objectcache/configurable');
        if ($cacheManager->cachedAllowProducts != null) {
            //Mage::log("Configurable getAllowProducts static cached copy!!", null, 'mylogfile.log');
            return $cacheManager->cachedAllowProducts;
        }

        if (!$this->hasAllowProducts()) {
            $products = array();
            $skipSaleableCheck = Mage::helper('catalog/product')->getSkipSaleableCheck();

            //Mage::log("Configurable getAllowProducts starting " . strval($this->getId()), null, 'mylogfile.log');
            $allProducts = $this->getProduct()->getTypeInstance(true)
                ->getUsedProducts(null, $this->getProduct());
            foreach ($allProducts as $product) {
                if ($product->isSaleable() || $skipSaleableCheck) {
                    $products[] = $product;
                }
            }
            $this->setAllowProducts($products);

            //Mage::log("Configurable getAllowProducts ended", null, 'mylogfile.log');
        } else {
            //Mage::log("Configurable getAllowProducts had a cached copy!!", null, 'mylogfile.log');
        }

        $cacheManager->cachedAllowProducts = $this->getData('allow_products');  //Cache in local static

        return $this->getData('allow_products');
    }

    public function getAllProducts()
    {
        //Try get from singleton cache manager
        $cacheManager = Mage::getSingleton('objectcache/configurable');
        if ($cacheManager->cachedAllProducts != null) {
            //Mage::log("Configurable getAllProducts static cached copy!!", null, 'mylogfile.log');
            return $cacheManager->cachedAllProducts;
        }

        if (!$this->hasAllowProductsAll()) {
            $products = array();

            //Mage::log("Configurable getAllProducts starting " . strval($this->getId()), null, 'mylogfile.log');
            $allProducts = $this->getProduct()->getTypeInstance(true)
                ->getUsedProducts(null, $this->getProduct());
            foreach ($allProducts as $product) {
                //Mage::log("allProducts product id: " . strval($product->getId()), null, 'mylogfile.log');
                if($product->getProductVariation($product) != null && $product->getCorrectConfigurableProductForSimpleProduct()->getStatus() == Mage_Catalog_Model_Product_Status::STATUS_DISABLED) {
                    continue;
                }

                $products[] = $product;
            }
            $this->setAllowProductsAll($products);

            //Mage::log("Configurable getAllProducts ended", null, 'mylogfile.log');
        } else {
            //Mage::log("Configurable getAllProducts had a cached copy!!", null, 'mylogfile.log');
        }

        $cacheManager->cachedAllProducts = $this->getData('allow_products_all');  //Cache in local static

        return $this->getData('allow_products_all');
    }

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfig()
    {
        //Try get from singleton cache manager
        $cacheManager = Mage::getSingleton('objectcache/configurable');
        if ($cacheManager->cachedJsonConfig != null) {
            //Mage::log("Configurable getJsonConfig static cached copy!!", null, 'mylogfile.log');
            return $cacheManager->cachedJsonConfig;
        }

        $attributes = array();
        $options    = array();
        $allowedProductsOptions    = array();
        $store      = $this->getCurrentStore();
        $coreHelper = Mage::helper('core');
        $taxHelper  = Mage::helper('tax');
        $weeHelper  = Mage::helper('weee');
        $currentProduct = $this->getProduct();

        $preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
        if ($preconfiguredFlag) {
            $preconfiguredValues = $currentProduct->getPreconfiguredValues();
            $defaultValues       = array();
        }

        //Modified to get additional R&R data
        $rrData = array();

        $localAllowAttributes = $this->getAllowAttributes();

        foreach ($this->getAllProducts() as $product) {
            $productId  = $product->getId();

            foreach ($localAllowAttributes as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
            }

            $rrData[$productId]['description'] = $product->getDescription();
            $rrData[$productId]['short_description'] = $product->getShortDescription();
            $rrData[$productId]['extra_text'] = $product->getExtraText();
            $rrData[$productId]['is_pre_order'] = $product->getIsPreOrder() ? true : false;
            $rrData[$productId]['product_care_info'] = $product->getProductCareInfo();
            $rrData[$productId]['form_url'] = $product->getTypeId() == 'simple' ? Mage::helper('checkout/cart')->getAddUrl($this->getConfigurableProduct($product)) : Mage::helper('checkout/cart')->getAddUrl($product);
        }

        foreach ($this->getAllowProducts() as $product) {
            $productId  = $product->getId();

            foreach ($localAllowAttributes as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($allowedProductsOptions[$productAttributeId])) {
                    $allowedProductsOptions[$productAttributeId] = array();
                }

                if (!isset($allowedProductsOptions[$productAttributeId][$attributeValue])) {
                    $allowedProductsOptions[$productAttributeId][$attributeValue] = array();
                }
                $allowedProductsOptions[$productAttributeId][$attributeValue][] = $productId;
            }
        }

        $this->_resPrices = array(
            $this->_preparePrice($currentProduct->getFinalPrice())
        );

        foreach ($localAllowAttributes as $attribute) {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();
            $info = array(
               'id'        => $productAttribute->getId(),
               'code'      => $productAttribute->getAttributeCode(),
               'label'     => $attribute->getLabel(),
               'options'   => array(),
            );

            $optionPrices = array();
            $prices = $attribute->getPrices();
            if (is_array($prices)) {
                foreach ($prices as $value) {
                    if(!$this->_validateAttributeValue($attributeId, $value, $options)) {
                        continue;
                    }
                    $currentProduct->setConfigurablePrice(
                        $this->_preparePrice($value['pricing_value'], $value['is_percent'])
                    );
                    $currentProduct->setParentId(true);
                    Mage::dispatchEvent(
                        'catalog_product_type_configurable_price',
                        array('product' => $currentProduct)
                    );
                    $configurablePrice = $currentProduct->getConfigurablePrice();

                    if (isset($options[$attributeId][$value['value_index']])) {
                        $productsIndex = $options[$attributeId][$value['value_index']];
                    } else {
                        $productsIndex = array();
                    }

                    if (isset($allowedProductsOptions[$attributeId][$value['value_index']])) {
                        $allowedProductsIndex = $allowedProductsOptions[$attributeId][$value['value_index']];
                    } else {
                        $allowedProductsIndex = array();
                    }

                    $associatedSimpleProductForAttributeId = null;

                    if($info['label'] == 'Variation' && $associatedSimpleProductForAttributeId == null) {
                        $associatedSimpleProductForAttributeId = Mage::getSingleton('objectcache/product')->get($productsIndex[0]);

                        $correctConfigurableProduct = $associatedSimpleProductForAttributeId->getCorrectConfigurableProductForSimpleProduct();
                        $configurablePrice = $correctConfigurableProduct->getFinalPrice() - $currentProduct->getFinalPrice();
                    }


                    $info['options'][] = array(
                        'id'        => $value['value_index'],
                        'label'     => $value['label'],
                        'price'     => $configurablePrice,
                        'oldPrice'  => $this->_prepareOldPrice($value['pricing_value'], $value['is_percent']),
                        'products'  => $allowedProductsIndex,
                        'allProducts'  => $productsIndex,
                    );

                    $optionPrices[] = $configurablePrice;
                }
            }
            /**
             * Prepare formated values for options choose
             */
            foreach ($optionPrices as $optionPrice) {
                foreach ($optionPrices as $additional) {
                    $this->_preparePrice(abs($additional-$optionPrice));
                }
            }
            if($this->_validateAttributeInfo($info)) {
               $attributes[$attributeId] = $info;
            }

            // Add attribute default value (if set)
            if ($preconfiguredFlag) {
                $configValue = $preconfiguredValues->getData('super_attribute/' . $attributeId);
                if ($configValue) {
                    $defaultValues[$attributeId] = $configValue;
                }
            }
        }

        $taxCalculation = Mage::getSingleton('tax/calculation');
        if (!$taxCalculation->getCustomer() && Mage::registry('current_customer')) {
            $taxCalculation->setCustomer(Mage::registry('current_customer'));
        }

        $_request = $taxCalculation->getDefaultRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $defaultTax = $taxCalculation->getRate($_request);

        $_request = $taxCalculation->getRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $currentTax = $taxCalculation->getRate($_request);

        $taxConfig = array(
            'includeTax'        => false,
            'showIncludeTax'    => false,
            'showBothPrices'    => false,
            'defaultTax'        => $defaultTax,
            'currentTax'        => $currentTax,
            'inclTaxTitle'      => Mage::helper('catalog')->__('Incl. Tax')
        );

        $config = array(
            'attributes'        => $attributes,
            'template'          => str_replace('%s', '#{price}', $store->getCurrentCurrency()->getOutputFormat()),
            'basePrice'         => $this->_registerJsPrice($this->_convertPrice($currentProduct->getFinalPrice())),
            'oldPrice'          => $this->_registerJsPrice($this->_convertPrice($currentProduct->getPrice())),
            'productId'         => $currentProduct->getId(),
            'chooseText'        => Mage::helper('catalog')->__('Choose an Option...'),
            'taxConfig'         => $taxConfig,
            'rrData'            => $rrData
        );

        if ($preconfiguredFlag && !empty($defaultValues)) {
            $config['defaultValues'] = $defaultValues;
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        $jsonConfigResult = Mage::helper('core')->jsonEncode($config);

        //Mage::log("Configurable getJsonConfig loaded", null, 'mylogfile.log');
        $cacheManager->cachedJsonConfig = $jsonConfigResult;  //Cache in local static

        return $jsonConfigResult;
    }

    private function getConfigurableProduct($simpleProductId)
    {
        $correctProduct = null;
        if(!is_object($simpleProductId) && is_numeric($simpleProductId)) {
            $_product = Mage::getSingleton('objectcache/product')->get($simpleProductId);
        } else {
            $_product = $simpleProductId;
        }

        $variation = $this->getProductVariation($_product);

        if ($variation == null) {
            return $_product;
        }

        // Find configurable product for this variation

        $configurable = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', 'configurable')
            ->addAttributeToFilter('default_selected_variation',array('eq' => $variation));

        $correctProduct = $configurable->getFirstItem();

        if ($correctProduct == null) {
            Mage::throwException('Could not find the correct product for and variation: '.$variation.' based on Default Selected Variation. Is it set on your configurable?');
        }

        return $correctProduct;
    }

    private function getProductVariation($_product, $labelToLookFor = 'Variation')
    {
        //Mage::log("RalphAndRusso_Catalog_Block_Product_View_Type_Configurable getProductVariation", null, 'mylogfile.log');
        $variation = null;
        $attributes = $_product->getAttributes();

        foreach ($attributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $label = $attribute->getStoreLabel();
            if ($label == $labelToLookFor) {
                $variation = $attribute->getFrontend()->getValue($_product);
                break;
            }
        }

        return $variation;
    }

}
