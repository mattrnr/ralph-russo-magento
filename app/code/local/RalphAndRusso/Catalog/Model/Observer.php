<?php
class RalphAndRusso_Catalog_Model_Observer
{
    const HAUTE_COUTURE_CATEGORY_ID = 60;

    const BOUTIQUES_CATEGORY_ID = 58;

    const THE_HOUSE_CATEGORY_ID = 62;

    const READY_TO_WEAR_CATEGORY_ID = 135;

    /**
    * Converts attribute set name of current product to nice name ([a-z0-9_]+).
    * Adds layout handle PRODUCT_ATTRIBUTE_SET_<attribute_set_nicename> after
    * PRODUCT_TYPE_<product_type_id> handle
    *
    * Event: controller_action_layout_load_before
    *
    * @param Varien_Event_Observer $observer
    */
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {

        $update = $observer->getEvent()->getLayout()->getUpdate();
        $id = Mage::app()->getRequest()->getParam('id');

        if ($id == null) {
            return;
        }

        $category = Mage::getModel('catalog/category')->load($id);

        if ($category == null) {
            return;
        }


        foreach ($category->getParentCategories() as $parent) {
            if ($parent->getId() == self::HAUTE_COUTURE_CATEGORY_ID &&
                stripos($_SERVER['REQUEST_URI'], "/lookbook") === false) {
                $update->addHandle('HAUTE_COUTURE');
                break;
            }


            if (stripos($_SERVER['REQUEST_URI'], "/shoes/autumn-winter-2016-2017-shoes-preview") !== false || stripos($_SERVER['REQUEST_URI'], "/handbags/autumn-winter-2016-2017-bags-preview") !== false) {
                $update->addHandle('PRODUCT_PREVIEW');
                break;
            }

            if ($parent->getId() == self::READY_TO_WEAR_CATEGORY_ID &&
            stripos($_SERVER['REQUEST_URI'], "/lookbook") !== false) {
                $update->addHandle('LOOKBOOK_CATEGORY');
                break;
            }


            if ($parent->getId() == self::HAUTE_COUTURE_CATEGORY_ID &&
            stripos($_SERVER['REQUEST_URI'], "/lookbook") !== false) {
                $update->addHandle('LOOKBOOK_CATEGORY');
                break;
            }


            if ($parent->getId() == self::BOUTIQUES_CATEGORY_ID) {
                $update->addHandle('BOUTIQUE');
                break;
            }

            if ($parent->getId() == self::THE_HOUSE_CATEGORY_ID) {
                $update->addHandle('THE_HOUSE');
                break;
            }
        }
    }
}
