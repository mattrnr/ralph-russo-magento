<?php

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$setup->addAttribute('catalog_product', 'short_spec', array(
    'group'         => 'General',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Short Specifications',
    'backend'       => '',
    'visible'       => true,
    'required'      => false,
    'user_defined' => true,
    'visible_on_front' => true,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->updateAttribute('catalog_product', 'short_spec', 'is_wysiwyg_enabled', 1);
$setup->updateAttribute('catalog_product', 'short_spec', 'is_html_allowed_on_front', 1);

$installer->endSetup(); 
