<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_CatalogSearch
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Autocomplete queries list
 */
class RalphAndRusso_CatalogSearch_Block_Autocomplete extends Mage_CatalogSearch_Block_Autocomplete
{

    protected $_suggestData = null;

    protected function _toHtml() {

        $html = '';

        if (!$this->_beforeToHtml()) {
            return $html;
        }

        $suggestData = $this->getSuggestData();

        if (count($suggestData) == 0) {
         return $html .= '<div id="no-result">YOUR SEARCH TURNS NO RESULTS. PLEASE, TRY AGAIN.</div>';
     } 


     $isAjaxSuggestionCountResultsEnabled = (bool) Mage::app()->getStore()
     ->getConfig(Mage_CatalogSearch_Model_Query::XML_PATH_AJAX_SUGGESTION_COUNT);

     $count--;

     $i = 0;

     $html = '<ul><li style="display:none"></li>';

     foreach ($suggestData as $index => $item) {

        if ($index == 0) {
            $item['row_class'] .= ' first';
        }

        if ($index == $count) {
            $item['row_class'] .= ' last';
        }

        $html .= '<li title="' . $this->escapeHtml($item['title']) . '" class="' . $item['row_class'] . '">';
        $html .= '<a href="' . $item["link"] . '">';
        $html .= '<img src="' . $item['image'] .'"/><div class="title">' . $this->escapeHtml($item['title']) .'</div></a></li>';

        $i++;
        if($i==6) break;
    }

    if(count($suggestData) > 6 ) {

        $catalogSearch = $this->helper('catalogsearch');
        $html .= '<li><a href="' . $catalogSearch->getResultUrl($catalogSearch->getQueryText()) . '" class="btn"> VIEW MORE </a></li>';
    }

    $html.= '</ul>';


    return $html;
}

public function getSuggestData()
{

    if (!$this->_suggestData) {
        $query = $this->helper('catalogsearch')->getQueryText();
        $counter = 0;
        $data = array();

              // Get products where the url matches the query in some meaningful way

        $products = Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToSelect(['name', 'image'])
        ->addAttributeToFilter('type_id', 'configurable')
        ->addAttributeToFilter('sku',array('like'=>'%'.$query.'%'))
        ->load();

        foreach($products as $product) {

            $_data = array(
                'title' => $product->getName(),
                'link' => $product->getUrlInStore(),
                'image' => $product->getImageUrl(),
                'row_class' => (++$counter)%2?'odd':'even',
                'num_of_results' => 1
                );

            $data[] = $_data;
        }

        $this->_suggestData = $data;

    }
    return $this->_suggestData;
}

/*
 *
*/
}
