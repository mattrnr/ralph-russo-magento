<?php
$installer = $this;
$installer->startSetup();
$productEntityTypeId = $installer->getEntityTypeId('catalog_category');
$installer->addAttribute('catalog_category', 'haute_couture_intro_video', array(
    'group'         => 'Couture',
    'input'         => 'textarea',
    'type'          => 'text',
    'label'         => 'Intro Video',
    'backend'        => 'eav/entity_attribute_backend_array',
    'visible'       => 1,
    'required'      => 0,
    'visible_on_front' => true,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->updateAttribute($productEntityTypeId, 'haute_couture_intro_video', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute($productEntityTypeId, 'haute_couture_intro_video', 'is_html_allowed_on_front', 1);

$installer->endSetup();
?>
