<?php
$installer = $this;
$installer->startSetup();
$productEntityTypeId = $installer->getEntityTypeId('catalog_category');
$installer->addAttribute('catalog_category', 'custom_menu_label', array(
    'group'         => 'Extra Options',
    'input'         => 'text',
    'type'          => 'text',
    'label'         => 'Menu Label',
    'backend'        => 'eav/entity_attribute_backend_array',
    'visible'       => 1,
    'required'      => 0,
    'visible_on_front' => true,
    'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));


$installer->endSetup();
?>
