<?php
/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magento.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magento.com for more information.
*
* @category    Mage
* @package     Mage_Contacts
* @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
* @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

/**
* Contacts index controller
*
* @category   Mage
* @package    Mage_Contacts
* @author      Magento Core Team <core@magentocommerce.com>
*/

include_once("Mage/Contacts/controllers/IndexController.php");

class RalphAndRusso_Contacts_IndexController extends Mage_Contacts_IndexController
{

    // const XML_PATH_EMAIL_RECIPIENT  = 'contacts/email/recipient_email';
    const XML_PATH_EMAIL_SENDER     = 'contacts/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE   = 'contacts/email/email_template';
    const XML_PATH_EMAIL_CUSTOMER_CONFIRMATION_TEMPLATE   = 'contacts/email/email_customer_confirmation_template';
    const XML_PATH_ENABLED          = 'contacts/contacts/enabled';

    public function preDispatch()
    {
        parent::preDispatch();

        if( !Mage::getStoreConfigFlag(self::XML_PATH_ENABLED) ) {
            $this->norouteAction();
        }
    }

    public function indexAction()
    {

        $this->loadLayout();

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Home', array('label'=>"Home", 'title'=>"Home", 'link'=>Mage::getBaseUrl()));
        $breadcrumbs->addCrumb('Contact us', array('label'=>"Contact us", 'title'=>"Contact us", 'link'=> "/contact-us"));

        $this->getLayout()->getBlock('contactForm')
        ->setFormAction( Mage::getUrl('*/*/post', array('_secure' => $this->getRequest()->isSecure())) );

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(trim($post['enquiry-type']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['title']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['first-name']) , 'NotEmpty')) {
                    $error = true;
                    $firstname = false;
                } else {
                  if (preg_match("/^[a-zA-Z]+$/", trim($post['first-name']))) {
                    $firstname = false;
                  } else {
                    $firstname = true;
                  }
                }

                if (!Zend_Validate::is(trim($post['last-name']) , 'NotEmpty')) {
                    $error = true;
                    $lastname = false;
                } else {
                  if (preg_match("/^[a-zA-Z]+$/", trim($post['last-name']))) {
                    $lastname = false;
                  } else {
                    $lastname = true;
                  }
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['phone']), 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['country']), 'NotEmpty')) {
                    $error = true;
                }


                if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }

                if ($firstname == false && $lastname == false) {
                    if (trim($post['first-name']) != trim($post['last-name'])) {
                        $zendesk = $this->zendeskPost($post, "Contact Us");
                        $translate->setTranslateInline(true);

                        if (!$zendesk) {
                            Mage::getSingleton('customer/session')->addSuccess('<h3>Thank you for contacting Ralph & Russo</h3><p>A member of our team will respond to your enquiry shortly.<br><br><span style="font-size:10px; font-style: italic;">*Based on uk holiday hours</span></p>');
                            $this->_redirect('*/*/');
                        } else {
                            Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. <br><span style="font-size: 9pt">' . $zendesk . '</span>'));
                            $this->_redirect('*/*/');
                        }
                    } else {
                        Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. <br><span style="font-size: 9pt">' . $zendesk . '</span>'));
                        $this->_redirect('*/*/');
                    }
                } else {
                  $translate->setTranslateInline(true);
                  Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request.'));
                  $this->_redirect('*/*/');
                  return;
                }

                return;
            } catch (Exception $e) {
                var_dump($e);
                $translate->setTranslateInline(true);

                Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }

}
