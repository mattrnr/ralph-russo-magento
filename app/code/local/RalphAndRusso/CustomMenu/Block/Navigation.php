<?php

class RalphAndRusso_CustomMenu_Block_Navigation extends WP_CustomMenu_Block_Navigation
{
	const CUSTOM_BLOCK_TEMPLATE = "wp_custom_menu_%d";

	private $_productsCount     = null;
	private $_topMenu           = array();
	private $_popupMenu         = array();

    public function drawCustomMenuMobileItem($category, $level = 0, $last = false)
    {
        if (!$category->getIsActive()) {
            return '';
        }
        $id = $category->getId();

        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "navigationCustomMenu_" . strval($id) . "_" . strval($level) . "_" . strval($last);
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***drawCustomMenuMobileItem cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $html = array();
        // --- Sub Categories ---
        $activeChildren = $this->_getActiveChildren($category, $level);
        // --- class for active category ---
        $active = ''; if ($this->isCategoryActive($category)) $active = ' act';
        $hasSubMenu = count($activeChildren);
        $catUrl = $this->getCategoryUrl($category);

        if(count($activeChildren) == 1) {
            $hasSubMenu = false;
            $catUrl = $this->getCategoryUrl($activeChildren[0]);
        }

        $html[] = '<div id="menu-mobile-' . $id . '" class="menu-mobile level0' . $active . '">';
        $html[] = '<div class="parentMenu">';
        // --- Top Menu Item ---
        if ($hasSubMenu) {
            $html[] = '<a class="level' . $level . 'sub-nav"' . ' rel="submenu-mobile-' . $id . '"'. ' onclick="wpSubMenuToggle(this, \'menu-mobile-' . $id . '\', \'submenu-mobile-' . $id . '\');">';
        }
        if (!$hasSubMenu) {
            $html[] = '<a class="level' . $level . '" href="' . $catUrl .'">';
        }

        $name = $this->escapeHtml($category->getName());
        if (Mage::getStoreConfig('custom_menu/general/non_breaking_space')) {
            $name = str_replace(' ', '&nbsp;', $name);
        }
        $html[] = '<span>' . $name . '</span>';


        $html[] = '</a>';
        $html[] = '</div>';
        // --- Add Popup block (hidden) ---
        if ($hasSubMenu) {
            // --- draw Sub Categories ---
            $html[] = '<div id="submenu-mobile-' . $id . '" rel="level' . $level . '" class="wp-custom-menu-submenu" style="display: none;">';
            $html[] = $this->drawMobileMenuItem($activeChildren);
            $html[] = '<div class="clearBoth"></div>';
            $html[] = '</div>';
        }
        $html[] = '</div>';
        $html = implode("\n", $html);

        $mageCache->save(serialize($html), $cacheKey, array('Navigation', 'CustomMenuMobileItem'), 3600);    //Add to the cache
        //Mage::log("drawCustomMenuMobileItem uncached " . $cacheKey, null, 'mylogfile.log');

        return $html;
    }

	public function drawMobileMenuItem($children, $level = 1)
	{
		$keyCurrent = $this->getCurrentCategory()->getId();
		$html = '';
		$iii = 0;
		foreach ($children as $child) {
			if (is_object($child) && $child->getIsActive()) {
				$iii++;

                $category = Mage::getSingleton('objectcache/category')->get($child->getId());
				$parentCat = $category->getParentCategory();
				$parentCatId = $parentCat ? $parentCat->getId() : -1;

	            // --- class for active category ---
				$active = '';
				$id = $child->getId();
				$activeChildren = $this->_getActiveChildren($child, $level);
				if ($this->isCategoryActive($child)) {
					$active = ' actParent';
					if ($id == $keyCurrent) $active = ' act';
				}
				$html.= '<div id="menu-mobile-' . $id . '" class="itemMenu level' . $level . $active . '">';
	            // --- format category name ---
				// $name = $this->escapeHtml($child->getName());

				$name = $category->getData("custom_menu_label");

				if ($name == null || $name == "")
					$name = $this->escapeHtml($category->getName());


				if (Mage::getStoreConfig('custom_menu/general/non_breaking_space')) $name = str_replace(' ', '&nbsp;', $name);
				$html.= '<div class="parentMenu">';

				if($parentCatId == "135" || $parentCatId == "57" || $parentCatId == "93" || $parentCatId == "175") {
					if ($iii == 1 || $iii == 2) {
						$html.= '<a class="itemMenuName level' . $level . $active . '" href="' . $this->getCategoryUrl($child) . '"><img manual-image-load data-src="' . $category->getThumbnailUrl() . '"/><span>' . $name . '</span></a>';
					} else {
						if ($iii == 3) {
							if($parentCatId == "135" || $parentCatId == "175") {
								$html.= '<div class="HC-archive"><span>SHOP COLLECTION</span></div>';
							} else {
								$html.= '<div class="HC-archive"><span>STYLES</span></div>';
							}
						}
						$html.= '<a class="mobile-rtw-item itemMenuName level' . $level . $active . '" href="' . str_replace("ready-to-wear","online-boutique-rtw", $this->getCategoryUrl($child)) . '"><img manual-image-load data-src="' . $category->getThumbnailUrl() . '"/><span>' . $name . '</span></a>';
					}
				} else {
					$html.= '<a class="itemMenuName level' . $level . $active . '" href="' . $this->getCategoryUrl($child) . '"><img manual-image-load data-src="' . $category->getThumbnailUrl() . '"/><span>' . $name . '</span></a>';
				}

				if (count($activeChildren) > 0) {
					$html.= '<span class="button" rel="submenu-mobile-' . $id . '" onclick="wpSubMenuToggle(this, \'menu-mobile-' . $id . '\', \'submenu-mobile-' . $id . '\');">&nbsp;</span>';
				}
				$html.= '</div>';
				if (count($activeChildren) > 0) {
					$html.= '<div id="submenu-mobile-' . $id . '" rel="level' . $level . '" class="wp-custom-menu-submenu level' . $level . '" style="display: none;">';
					$html.= $this->drawMobileMenuItem($activeChildren, $level + 1);
					$html.= '<div class="clearBoth"></div>';
					$html.= '</div>';
				}
				$html.= '</div>';
			}
		}
		return $html;
	}


	public function getTopMenuArray()
	{
		return $this->_topMenu;
	}

	public function getPopupMenuArray()
	{
		return $this->_popupMenu;
	}

	public function drawCustomMenuItem($category, $level = 0, $last = false)
	{
        if (!$category->getIsActive()) return;
        $id = $category->getId();

        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "navigationCustomMenuItem_" . strval($id) . "_" . strval($level) . "_" . strval($last);
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
            //Mage::log("***drawCustomMenuItem cached " . $cacheKey null, 'mylogfile.log');
            $cachedCustomMenuItem = Mage::getModel('objectcache/cachedcustommenuitem');
            $cachedCustomMenuItem = unserialize($cachedData);
        } else {
            $cachedCustomMenuItem = Mage::getModel('objectcache/cachedcustommenuitem');

            // --- Static Block ---
            $blockId = sprintf(self::CUSTOM_BLOCK_TEMPLATE, $id); // --- static block key
            $collection = Mage::getModel('cms/block')->getCollection()
                ->addFieldToFilter('identifier', array(array('like' => $blockId . '_w%'), array('eq' => $blockId)))
                ->addFieldToFilter('is_active', 1);
            $blockId = $collection->getFirstItem()->getIdentifier();
            $blockHtml = Mage::app()->getLayout()->createBlock('cms/block')->setBlockId($blockId)->toHtml();
            // --- Sub Categories ---
            $activeChildren = $this->_getActiveChildren($category, $level);
            // --- class for active category ---
            $active = '';
            if ($this->isCategoryActive($category)) $active = ' act';
            // --- Popup functions for show ---
            $cachedCustomMenuItem->drawPopup = ($blockHtml || count($activeChildren));

            $categoryUrl = $this->getCategoryUrl($category);

            if (count($activeChildren) == 1) {
                $cachedCustomMenuItem->drawPopup = false;
                $categoryUrl = $this->getCategoryUrl($activeChildren[0]);
            }

            $htmlTop = array();
            if ($cachedCustomMenuItem->drawPopup) {
                $htmlTop[] = '<div id="menu' . $id . '" class="menu' . $active . '" onclick="wpToggleDesktopMenuPopup(this, event, \'popup' . $id . '\', \'menu' . $id . '\');">';
            } else {
                $htmlTop[] = '<div id="menu' . $id . '" class="menu' . $active . '">';
            }
            // --- Top Menu Item ---
            $htmlTop[] = '<div class="parentMenu">';
            if ($level == 0 && $cachedCustomMenuItem->drawPopup) {
                $htmlTop[] = '<a  class="level' . $level . $active . '" href="javascript:void(0);" rel="' . $categoryUrl . '">';
            } else {
                $htmlTop[] = '<a  class="level' . $level . $active . '" href="' . $categoryUrl . '">';
            }
            $name = $this->escapeHtml($category->getName());
            if (Mage::getStoreConfig('custom_menu/general/non_breaking_space')) {
                $name = str_replace(' ', '&nbsp;', $name);
            }

            $htmlTop[] = '<span>' . $name . '</span>';
            $htmlTop[] = '</a>';
            $htmlTop[] = '</div>';
            $htmlTop[] = '</div>';
            $cachedCustomMenuItem->htmlTopData = implode("\n", $htmlTop);

            // --- Add Popup block (hidden) ---
            $cachedCustomMenuItem->htmlPopupData = '';
            if ($cachedCustomMenuItem->drawPopup) {
                $htmlPopup = array();
                //     // --- Popup function for hide ---
                $htmlPopup[] = '<div id="popup' . $id . '" class="wp-custom-menu-popup">';

                // --- draw Sub Categories ---
                if (count($activeChildren)) {

                    $columns = (int)Mage::getStoreConfig('custom_menu/columns/count');
                    $htmlPopup[] = '<div class="custom-menu-wrapper">';
                    $htmlPopup[] = '<div class="close-character" onclick="wpHideMenuPopup(this, event, \'popup' . $id . '\', \'menu' . $id . '\')"></div>';
                    $htmlPopup[] = '<div class="inner-container single-col">';
                    $htmlPopup[] = '<div class="block1">';
                    $htmlPopup[] = $this->drawColumns($activeChildren, 1);
                    $htmlPopup[] = '<div class="clearBoth"></div>';
                    $htmlPopup[] = '</div>';
                    $htmlPopup[] = '</div>';
                    $htmlPopup[] = '</div>';

                }
                // --- draw Custom User Block ---
                if ($blockHtml) {
                    $htmlPopup[] = '<div id="' . $blockId . '" class="block2">';
                    $htmlPopup[] = $blockHtml;
                    $htmlPopup[] = '</div>';
                }
                $htmlPopup[] = '</div>';
                $cachedCustomMenuItem->htmlPopupData = implode("\n", $htmlPopup);
            }

            $mageCache->save(serialize($cachedCustomMenuItem), $cacheKey, array('Navigation', 'CustomMenuItem'), 3600);    //Add to the cache
//            Mage::log("drawCustomMenuItem uncached " . $cacheKey, null, 'mylogfile.log');
        }

        $this->_topMenu[] = $cachedCustomMenuItem->htmlTopData;
        if ($cachedCustomMenuItem->drawPopup) {
            $this->_popupMenu[] = $cachedCustomMenuItem->htmlPopupData;
        }
	}



	public function drawMenuItem($children, $level = 1)
	{
		$keyCurrent = $this->getCurrentCategory()->getId();

		$html = '<div class="itemMenu level' . $level . ' uk-grid uk-container-center">';

		$i = 0;
		foreach ($children as $child) {

			$i++;

			if (is_object($child) && $child->getIsActive()) {

                $category = Mage::getSingleton('objectcache/category')->get($child->getId());
				$parentCat = $category->getParentCategory();

				$parentCatId = $parentCat ? $parentCat->getId() : -1;


				// --- class for active category ---
				$active = '';
				if ($this->isCategoryActive($child)) {
					$active = ' actParent';
					if ($child->getId() == $keyCurrent) $active = ' act';
				}
				// --- format category name ---

				$activeChildren = $this->_getActiveChildren($child, $level);
				$hasActiveChildren = count($activeChildren) > 0;

				if (Mage::getStoreConfig('custom_menu/general/non_breaking_space')) $name = str_replace(' ', '&nbsp;', $name);

				$name = $category->getData("custom_menu_label");

				if ($name == null || $name == "")
					$name = $this->escapeHtml($child->getName());


				if($parentCatId == "60" || $parentCatId == "135" || $parentCatId == "57" || $parentCatId == "93" || $parentCatId == "175") {
					if($i == 1 || $i == 2) {
						$html.= '<div class="itemMenuName level' . $level . $active . ' uk-width-1-3 ">';
						$html.= '<a href="' . $this->getCategoryUrl($child) . '"><img src="' . $category->getThumbnailUrl() . '"/><div class="name"><span>' . $name . '</span></div></a>';
						$html.= '</div>';
					} else {
						if($parentCatId == "135" || $parentCatId == "175") {
							$html.= '<div class="inline-name"><a class="name" href="' . str_replace("ready-to-wear","online-boutique-rtw", $this->getCategoryUrl($child)) . '"><span>' . $name . '</span></a></div>';
						} else {
							if($i == 3 && $parentCatId == "57" || $i == 3 && $parentCatId == "93") {
								$html.= '<div class="inline-name"><a class="name archive" href="javascript:void(0)"><span>STYLES</span></a></div>';
							}
							$html.= '<div class="inline-name"><a class="name" href="' . $this->getCategoryUrl($child) . '"><span>' . $name . '</span></a></div>';
						}
					}

					if ($hasActiveChildren) {
						$html.= '<div class="itemSubMenu level' . $level . '">';
						$html.= $this->drawMenuItem($activeChildren, $level + 1);
						$html.= '</div>';
					}
				} else {
					$html.= '<div class="itemMenuName level' . $level . $active . ' uk-width-1-4 ">';
					$html.= '<a href="' . $this->getCategoryUrl($child) . '"><img src="' . $category->getThumbnailUrl() . '"/><div class="name"><span>' . $name . '</span></div></a>';
					$html.= '</div>';

					if ($hasActiveChildren) {
						$html.= '<div class="itemSubMenu level' . $level . '">';
						$html.= $this->drawMenuItem($activeChildren, $level + 1);
						$html.= '</div>';
					}

				}


			}
		}
		$html.= '</div>';
		return $html;
	}
}
