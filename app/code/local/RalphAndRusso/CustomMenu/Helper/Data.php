<?php

class RalphAndRusso_CustomMenu_Helper_Data extends WP_CustomMenu_Helper_Data
{
    private $_menuData = null;

    public function getMobileMenuContent()
    {
        $menuData = Mage::helper('custommenu')->getMenuData();
        extract($menuData);

        // --- Home Link ---
        $homeLinkUrl        = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $homeLinkText       = $this->__('Home');
        $homeLink           = '';
        if ($_showHomeLink) {
            $homeLink = <<<HTML
<div id="menu-mobile-0" class="menu-mobile level0">
    <div class="parentMenu">
        <a href="$homeLinkUrl">
            <span>$homeLinkText</span>
        </a>
    </div>
</div>
HTML;
        }
        // --- Menu Content ---
        $mobileMenuContent = '';
        $mobileMenuContentArray = array();
        foreach ($_categories as $_category) {
            $mobileMenuContentArray[] = $_block->drawCustomMenuMobileItem($_category);
        }

        if (count($mobileMenuContentArray)) {
            $mobileMenuContent = implode("\n", $mobileMenuContentArray);
        }


        $mobileMenuContent .= <<<HTML
<div id="menu-mobile-1000" class="menu-mobile level0">
<div class="parentMenu">
    <a class="static-link" href="/contact-us">
        <span>Contact Us</span>
    </a>
</div>
</div>
<div id="menu-mobile-1001" class="menu-mobile level0">
<div class="parentMenu">
    <a class="static-link" data-uk-modal="{target:'#newsletter-modal', center:false}" href="javascript:void(0)">
        <span>Subscribe to the newsletter</span>
    </a>
</div>
</div>
HTML;
        // --- Result ---
        $menu = <<<HTML
$homeLink
$mobileMenuContent
<div class="clearBoth"></div>
HTML;
        return $menu;
    }
}
