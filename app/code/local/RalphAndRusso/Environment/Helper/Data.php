<?php

class RalphAndRusso_Environment_Helper_Data extends Mage_Core_Helper_Abstract {

   public function getIsProductionEnvironment()
   {
       return strripos($_SERVER['SERVER_NAME'], "ralphandrusso.com") !== false;
   }

}
