<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Full Page Cache
 * @version   1.0.17
 * @build     612
 * @copyright Copyright (C) 2016 Mirasvit (http://mirasvit.com/)
 */



class RalphAndRusso_Fpc_Helper_Processor_Requestcacheid extends Mirasvit_Fpc_Helper_Processor_Requestcacheid
{
    /**
     * @var bool|array
     */
    protected $_custom;

    public function __construct()
    {
        $this->_custom = Mage::helper('fpc/custom')->getCustomSettings();
    }

    /**
     * Cache id for current request (md5)
     *
     * @return string
     */
    public function getRequestCacheId()
    {
        return Mirasvit_Fpc_Model_Config::REQUEST_ID_PREFIX . md5($this->_getRequestId());
    }

    /**
     * Build request id for current request
     *
     * @return string
     */
    protected function _getRequestId()
    {
        if ($customerId = $this->_getLoggedCustomerId()) {
            $customerGroupId =  $this->_getCustomerGroupId($customerId); //for logged in user
        } else {
            $customerGroupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        }

        $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode(); // Mage::getStoreConfig('currency/options/default', Mage::app()->getStore()->getId());

        $url = Mage::helper('fpc')->getNormalizedUrl();

        $dependencies = array(
            $url,
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('layout'),
            Mage::app()->getStore()->getCode(),
            Mage::app()->getLocale()->getLocaleCode(),
            $currentCurrencyCode,
            $customerGroupId,
            intval(Mage::app()->getRequest()->isXmlHttpRequest()),
            Mage::app()->getStore()->isCurrentlySecure(),
            Mage::getSingleton('core/design_package')->getTheme('frontend'),
            Mage::getSingleton('core/design_package')->getPackageName(),
            $this->getPriceIncludesTax()
        );

        $action = Mage::helper('fpc')->getFullActionCode();

        switch ($action) {
            case 'catalog/category_view':
            case 'splash/page_view':
                $data = Mage::getSingleton('catalog/session')->getData();
                $paramsMap = array(
                    'display_mode'   => 'mode',
                    'limit_page'     => 'limit',
                    'sort_order'     => 'order',
                    'sort_direction' => 'dir',
                );
                foreach ($paramsMap as $sessionParam => $queryParam) {
                    if (isset($data[$sessionParam])) {
                        $dependencies[] = $queryParam . '_' . $data[$sessionParam];
                    }
                }
                break;
        }

        foreach ($this->getConfig()->getUserAgentSegmentation() as $segment) {
            if ($segment['useragent_regexp']
                && preg_match($segment['useragent_regexp'], Mage::helper('core/http')->getHttpUserAgent())) {
                    $dependencies[] = $segment['cache_group'];
            }
        }

        if (Mage::helper('mstcore')->isModuleInstalled('AW_Mobile2')
            && Mage::helper('aw_mobile2')->isCanShowMobileVersion()
        ) {
            $dependencies[] = 'awMobileGroup';
        }

        if (Mage::helper('mstcore')->isModuleInstalled('Mediarocks_RetinaImages')
            && Mage::getStoreConfig('retinaimages/module/enabled')) {
                $retinaValue  = Mage::getModel('core/cookie')->get('device_pixel_ratio');
                $dependencies[] = (!$retinaValue) ? false : $retinaValue;
        }

        if ($deviceType = Mage::helper('fpc/mobile')->getMobileDeviceType()) {
            $dependencies[] = $deviceType;
        }

        if ($this->_custom && in_array('getRequestIdDependencies', $this->_custom)) {
            $dependencies[] = Mage::helper('fpc/customDependence')->getRequestIdDependencies();
        }

        $requestId = strtolower(implode('/', $dependencies));

        if ($this->getConfig()->isDebugLogEnabled()) {
            Mage::log('Request ID: ' . $requestId, null, Mirasvit_Fpc_Model_Config::DEBUG_LOG);
        }


        return $requestId;
    }

    protected function getPriceIncludesTax()
    {
        $taxRequest  = new Varien_Object();
        $taxRequest->setCountryId(Mage::getModel('geoip/geoip')->getCountryCode());
        $taxRequest->setCustomerClassId(7); // Directly set to 7 as General Customer
        $taxRequest->setProductClassId(2); // Directly set to 2 as VAT UK Standard
        $taxRequest->setStore(Mage::app()->getStore());
        $taxCalculationModel = Mage::getSingleton('tax/calculation');
        $currentTaxRate = $taxCalculationModel->getRate($taxRequest);

        return $currentTaxRate != 0;
    }
}
