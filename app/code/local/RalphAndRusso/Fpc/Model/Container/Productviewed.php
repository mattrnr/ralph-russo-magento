<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at http://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   Full Page Cache
 * @version   1.0.17
 * @build     612
 * @copyright Copyright (C) 2016 Mirasvit (http://mirasvit.com/)
 */



class RalphAndRusso_Fpc_Model_Container_Productviewed extends Mirasvit_Fpc_Model_Container_Productviewed
{
    protected function _renderBlock()
    {
        $layout = new Inchoo_PHP7_Model_Layout($this->_definition['layout']);
        $layout->generateBlocks();
        $block = $layout->getBlock($this->_definition['block_name']);

        if ($block) {
            $block->setProductIds($this->_getProductIds());
            $collection = $block->getItemsCollection();

            // set correct order
            if (is_object($collection)) {
                foreach ($this->_getProductIds() as $productId) {
                    $item = $collection->getItemById($productId);
                    $collection->removeItemByKey($productId);
                    if ($item) {
                        $collection->addItem($item);
                    }
                }
            }

            $html = $block->toHtml();
        } else {
            $html = '';
        }

        return $html;
    }
}
