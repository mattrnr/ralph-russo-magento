<?php

if(!function_exists('geoip_country_code_by_name') && Mage::helper("geoip")->getConfig('general/apache_or_file') == 1){
    define('GEOIP_LOCAL',1);
    $geoIpInc = Mage::getBaseDir('lib').DS.'Juicy'.DS.'Geoip'.DS.'geoip.inc';
    include($geoIpInc);
}

class RalphAndRusso_Geoip_Model_Geoip extends Juicy_Geoip_Model_Geoip
{

    public function getCountryCode() {
         return $this->_getCountryCode();
    }

    public function getCountryCodeFromIP($ip)
    {
        return $this->_getCountryCodeFromIp($ip);
    }

}
