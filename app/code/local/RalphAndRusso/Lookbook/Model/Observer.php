<?php
class RalphAndRusso_Lookbook_Model_Observer
{
    /**
    * Converts attribute set name of current product to nice name ([a-z0-9_]+).
    * Adds layout handle PRODUCT_ATTRIBUTE_SET_<attribute_set_nicename> after
    * PRODUCT_TYPE_<product_type_id> handle
    *
    * Event: controller_action_layout_load_before
    *
    * @param Varien_Event_Observer $observer
    */
    public function addAttributeSetHandle(Varien_Event_Observer $observer)
    {

        $update = $observer->getEvent()->getLayout()->getUpdate();

        if (stripos($_SERVER['REQUEST_URI'], "haute-couture") > -1 &&
        stripos($_SERVER['REQUEST_URI'], "/lookbook") === false) {
            $update->addHandle('HAUTE_COUTURE');
        } else if (stripos($_SERVER['REQUEST_URI'], "/lookbook") > -1){
            $update->addHandle('LOOKBOOK');
        } else if (stripos($_SERVER['REQUEST_URI'], "/the-house") > -1){
            $update->addHandle('THE_HOUSE');
        } else if (stripos($_SERVER['REQUEST_URI'], "ready-to-wear") &&
        stripos($_SERVER['REQUEST_URI'], "/lookbook") === false) {
            $update->addHandle('HAUTE_COUTURE');
        }
    }
}
