<?php


class RalphAndRusso_Lookbook_IndexController extends Mage_Core_Controller_Front_Action {

    public function indexAction() {

        $this->loadLayout();

        $breadcrumbs = $this->getLayout()->getBlock('breadcrumbs');
        $breadcrumbs->addCrumb('Home', array('label'=>"Home", 'title'=>"Home", 'link'=>Mage::getBaseUrl()));
        $breadcrumbs->addCrumb('Couture', array('readonly' => true, 'label'=>"Autumn / Winter Collection 2015", 'title'=>"Autumn / Winter Collection 2015"));
        $breadcrumbs->addCrumb('', array('label'=>"Lookbook", 'readonly' => true, 'title'=>"Lookbook", 'last' => true));

        $this->renderLayout();
    }
}


?>
