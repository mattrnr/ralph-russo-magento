<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Contacts
 * @copyright  Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Contacts index controller
 *
 * @category   Mage
 * @package    Mage_Contacts
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class RalphAndRusso_Newsletter_IndexController extends Mage_Core_Controller_Front_Action
{

    public function subscribeAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $postObject = new Varien_Object();
            $postObject->setData($post);

            $error = false;
            $errors = array();

            if (!Zend_Validate::is(trim($post['name']) , 'NotEmpty')) {
                $error = true;
            }

            if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                $error = true;
                array_push($errors, array('field' => 'newsletter-email', 'type' => 'validate-email', 'message' => 'Email address is not valid.'));
            }

            if (!Zend_Validate::is(trim($post['country']), 'NotEmpty')) {
                $error = true;
            }


            if (Zend_Validate::is(trim($post['hideit']), 'NotEmpty')) {
                $error = true;
            }

            if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
                $session            = Mage::getSingleton('core/session');
                $customerSession    = Mage::getSingleton('customer/session');
                $email              = (string) $this->getRequest()->getPost('email');

                if(Mage::getModel('newsletter/subscriber')->loadByEmail($email)->getId()) {
                    $error = true;
                    array_push($errors, array('field' => 'newsletter-email', 'type' => 'validate-email', 'message' => 'Email address is already subscribed.'));
                }

                if (!$error) {

                    $fullname = (string) $this->getRequest()->getPost('name');
                    $fullnameParts = explode(' ', $fullname, 2);
                    $firstName = $fullnameParts[0];
                    $lastName = isset($fullnameParts[1]) ? $fullnameParts[1] : null;
                    $country = $post['country'];

                    $status = Mage::getModel('newsletter/subscriber')->subscribe($email, $firstName, $lastName, $country);
                }
            }

            if ($error) {
                echo json_encode(array('response' => 'ERROR', 'errors' => $errors));
                return;
            }

            echo json_encode(array('response' => 'SUCCESS', 'message' => '<h3>THANKS FOR SIGNING UP</h3><p>You have successfully signed up to our newsletters</p>'));
            return;
        }
    }

}
