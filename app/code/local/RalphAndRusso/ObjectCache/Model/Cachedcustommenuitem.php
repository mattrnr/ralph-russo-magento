<?php

class RalphAndRusso_ObjectCache_Model_CachedCustomMenuItem
{
    public $htmlTopData;
    public $drawPopup;
    public $htmlPopupData;

    protected function _construct()
    {
        $this->_init('objectcache/cachedcustommenuitem');
    }
}