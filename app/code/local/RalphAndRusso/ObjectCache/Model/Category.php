<?php

/**
 * Class RalphAndRusso_ObjectCache_Model_Category
 * A simple cache of keys in an array (HashMap) for categories.
 * Use it by calling as shown because you want a singleton so the cache object is retained
 *      Mage::getSingleton('objectcache/category');
 */
class RalphAndRusso_ObjectCache_Model_Category extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('objectcache/category');
    }

    /**
     * Get an item by id
     * @param $id
     * @return Mage_Core_Model_Abstract|mixed
     */
    public function get($id) {
        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "Category_" . strval($id) . "_" . Mage::app()->getStore()->getCurrentCurrencyCode();
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***Category cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $category = Mage::getModel('catalog/category')->load($id);

        $mageCache->save(serialize($category), $cacheKey, array('Category'), 3600);    //cache for 60 mins
//        Mage::log("Category uncached " . $cacheKey, null, 'mylogfile.log');
        return $category;
    }
}