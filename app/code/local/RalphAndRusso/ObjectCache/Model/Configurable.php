<?php

/**
 * Class RalphAndRusso_ObjectCache_Model_Configurable
 * A simple cache of a single item - used with "Configurable" objects
 * Use it by calling as shown because you want a singleton so the cache object is retained
 *      Mage::getSingleton('objectcache/configurable');
 */
class RalphAndRusso_ObjectCache_Model_Configurable extends Mage_Core_Model_Abstract
{
    public $cachedAllowProducts;
    public $cachedAllProducts;
    public $cachedJsonConfig;

    protected function _construct()
    {
        $this->_init('objectcache/configurable');
    }
}