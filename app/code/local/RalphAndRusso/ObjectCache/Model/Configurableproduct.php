<?php

/**
 * Class RalphAndRusso_ObjectCache_Model_ConfigurableProduct
 * A simple cache of keys in an array (HashMap) for configurable products.
 * Use it by calling as shown because you want a singleton so the cache object is retained
 *      Mage::getSingleton('objectcache/configurableproduct');
 */
class RalphAndRusso_ObjectCache_Model_ConfigurableProduct extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('objectcache/configurableproduct');
    }

    /**
     * Get by product id
     * @param $product
     * @return Mage_Core_Model_Abstract|mixed
     */
    public function get($product) {
        $id = $product->getId();

        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "ConfigurableProduct_" . strval($id) . "_" . Mage::app()->getStore()->getCurrentCurrencyCode() . "_" . Mage::app()->getStore()->getCurrentCurrencyCode();
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***ConfigurableProduct cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $productType = $product->getTypeId();

        if($productType != 'simple')
            Mage::throwException("Product " . $product->getId() . " is not a simple product.");

        $variation = $product->getProductVariation($product);

        if ($variation == null) {
            Mage::throwException("Variation not found for product id " . $product->getId());
        }

        // Find configurable product for this variation
        $configurable = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id','configurable');

        $correctProduct = null;
        foreach ($configurable as $product1) {
            $variationFromCandidate = $product->getProductVariation($product1, "Default Selected Variation");

            if ($variation == $variationFromCandidate) {
                $correctProduct = $product1;
                break;
            }
        }
        if ($correctProduct == null) {
            Mage::throwException("Could not find the correct product for product id " . $id . " and variation: " . $variation . " based on Default Selected Variation. Is it set on your configurable?");
        }

        $mageCache->save(serialize($correctProduct), $cacheKey, array('Product', 'Configurable'), 3600);    //cache for 60 mins
//        Mage::log("ConfigurableProduct uncached " . $cacheKey, null, 'mylogfile.log');
        return $correctProduct;
    }
}