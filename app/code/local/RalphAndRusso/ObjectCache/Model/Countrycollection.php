<?php

class RalphAndRusso_ObjectCache_Model_CountryCollection {
    protected function _construct()
    {
        $this->_init('objectcache/countrycollection');
    }

    public function get() {
        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "countryCollection";
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***countryCollection cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $countries = Mage::getResourceModel('directory/country_collection')
            ->loadData()
            ->toOptionArray(false);
        $mageCache->save(serialize($countries), $cacheKey, array('CountryCollection'), 604800);    //Add to the cache for 1 week
//        Mage::log("countryCollection uncached " . $cacheKey, null, 'mylogfile.log');
        return $countries;
    }
}