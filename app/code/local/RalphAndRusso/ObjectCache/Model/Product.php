<?php

class RalphAndRusso_ObjectCache_Model_Product extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('objectcache/product');
    }

    /**
     * Get a product by id
     * @param $id
     * @return mixed|null
     * @throws Exception
     */
    public function get($id) {
        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "Product_" . strval($id) . "_" . Mage::app()->getStore()->getCurrentCurrencyCode();
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***Product cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $product = Mage::getModel('catalog/product')->load($id);

        $mageCache->save(serialize($product), $cacheKey, array('Product'), 3600);    //cache for 60 mins
//        Mage::log("Product uncached " . $cacheKey, null, 'mylogfile.log');
        return $product;
    }
}