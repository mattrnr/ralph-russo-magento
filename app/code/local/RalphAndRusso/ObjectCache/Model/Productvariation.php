<?php

/**
 * Class RalphAndRusso_ObjectCache_Model_ProductVariation
 * A simple cache of keys in an array (HashMap) for product variations.
 * Use it by calling as shown because you want a singleton so the cache object is retained
 *      Mage::getSingleton('objectcache/productvariation');
 */
class RalphAndRusso_ObjectCache_Model_ProductVariation extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('objectcache/productvariation');
    }

    /**
     * Get by product and label
     * @param $_product
     * @param $labelToLookFor
     * @return mixed|null
     */
    public function get($_product, $labelToLookFor) {
        $mageCache = Mage::getModel('core/cache');
        $cacheKey = "ProductVariation_" . strval($_product->getId()) . "_" . $labelToLookFor . "_" . Mage::app()->getStore()->getCurrentCurrencyCode();
        $cachedData = $mageCache->load($cacheKey);
        if ($cachedData != null) {
//            Mage::log("***ProductVariation cached " . $cacheKey, null, 'mylogfile.log');
            return unserialize($cachedData);
        }

        $variation = null;
        $attributes = $_product->getAttributes();
        foreach ($attributes as $attribute) {
            $attributeCode = $attribute->getAttributeCode();
            $label = $attribute->getStoreLabel();
            if ($label == $labelToLookFor) {
                $variation = $attribute->getFrontend()->getValue($_product);
                break;
            }
        }

        $mageCache->save(serialize($variation), $cacheKey, array('Product', 'Variation'), 3600);    //cache for 60 mins
//        Mage::log("ProductVariation uncached " . $cacheKey, null, 'mylogfile.log');
        return $variation;
    }
}