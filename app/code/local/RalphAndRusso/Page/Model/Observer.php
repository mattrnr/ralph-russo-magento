<?php

class RalphAndRusso_Page_Model_Observer
{

public function regenerateMenu($observer)
{
  // if module is active
  if (!Mage::getStoreConfig('advanced/modules_disable_output/Company_ModuleName'))
  {
    $layout = Mage::getSingleton('core/layout');

    // remove all the blocks you don't want
    $layout->getUpdate()->addUpdate('<remove name="catalog.topnav"/>');

    // load layout updates by specified handles
    $layout->getUpdate()->load();

    // generate xml from collected text updates
    $layout->generateXml();

    // generate blocks from xml layout
    $layout->generateBlocks();

  }
}
