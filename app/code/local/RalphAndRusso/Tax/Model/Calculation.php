<?php

class RalphAndRusso_Tax_Model_Calculation extends Mage_Tax_Model_Calculation
{

    /**
     * Get request object with information necessary for getting tax rate
     * Request object contain:
     *  country_id (->getCountryId())
     *  region_id (->getRegionId())
     *  postcode (->getPostcode())
     *  customer_class_id (->getCustomerClassId())
     *  store (->getStore())
     *
     * @param   null|false|Varien_Object $shippingAddress
     * @param   null|false|Varien_Object $billingAddress
     * @param   null|int $customerTaxClass
     * @param   null|int $store
     * @return  Varien_Object
     */
    public function getRateRequest(
        $shippingAddress = null,
        $billingAddress = null,
        $customerTaxClass = null,
        $store = null)
    {

        $request = parent::getRateRequest($shippingAddress, $billingAddress, $customerTaxClass, $store);

        if(Mage::helper("geoip")->isEnabled()) {
            $countryCode = Mage::getModel('geoip/geoip')->getCountryCode();

            if ($countryCode) {
                if (((is_null($shippingAddress)) && is_null($billingAddress)) || ($shippingAddress === false && $billingAddress === false)) {
                    $request->setCountryId($countryCode);
                    $request->setRegionId(0);
                    $request->setPostcode('*');
                } elseif (!$shippingAddress->getCountryId() && !$billingAddress->getCountryId()) {
                    $request->setCountryId($countryCode);
                    $request->setRegionId(0);
                    $request->setPostcode('*');
                }
            }
        }

        return $request;
    }

}
