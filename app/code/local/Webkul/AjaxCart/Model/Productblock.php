<?php
class Webkul_AjaxCart_Model_Productblock extends Mage_Core_Model_Abstract{

	public function getAllBlocks($product_id)
	{
        $finalHtml=array();
		$product = Mage::getModel('catalog/product')->load($product_id);
        $_url = Mage::helper('checkout/cart')->getAddUrl($product);
        $finalHtml['cart_url']=$_url;
        Mage::register('product', $product);
        Mage::register('current_product',$product);
        $layout = Mage::app()->getLayout();
        $finalHtml['option_Price']=json_decode($this->getJsonConfig($product_id));
        $finalHtml['product_name']=$product->getName();
        $finalHtml['image_url'] = $product->getImageUrl();
		if($product->getTypeId() == 'simple' || $product->getTypeId() == 'virtual'){
			if($product->getHasOptions()){
                $finalHtml['has_option']=1;
                $wrapper_block = $layout->createBlock('catalog/product_view')->setTemplate('ajaxcart/catalog/product/view/options/wrapper.phtml');
                
                $option_js_block = $layout->createBlock('core/template')->setTemplate('catalog/product/view/options/js.phtml');

                $options_block = $layout->createBlock('catalog/product_view_options',"product.info.options")->setTemplate('catalog/product/view/options.phtml');

                $options_block->addOptionRenderer('select','catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml');
                $options_block->addOptionRenderer('text','catalog/product_view_options_type_text','catalog/product/view/options/type/text.phtml');
                $options_block->addOptionRenderer('file','catalog/product_view_options_type_file','catalog/product/view/options/type/file.phtml');
                $options_block->addOptionRenderer('date','catalog/product_view_options_type_date','catalog/product/view/options/type/date.phtml');

                $wrapper_block->setChild("option_js",$option_js_block);
                $wrapper_block->setChild("option_wrapper",$options_block);


                $finalHtml['custom_option']=$wrapper_block->toHtml();

                $price_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/price_clone.phtml');
                $finalHtml['price']=$price_block->toHtml();

                $add_To_cart_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/addtocart.phtml');
                $finalHtml['add_to_cart']= $add_To_cart_block->toHtml();
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;
			}else{
                $finalHtml['has_option']=0;
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;
            }
		}else if($product->getTypeId() == 'configurable'){
                $finalHtml['has_option']=1;
                $wrapper_block = $layout->createBlock('catalog/product_view')->setTemplate('ajaxcart/catalog/product/view/options/wrapper.phtml');
                if($product->getHasOptions())
                {
                    $option_js_block = $layout->createBlock('core/template')->setTemplate('catalog/product/view/options/js.phtml');

                    $options_block = $layout->createBlock('catalog/product_view_options',"product.info.options")->setTemplate('catalog/product/view/options.phtml');

                    $options_block->addOptionRenderer('select','catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml');
                    $options_block->addOptionRenderer('text','catalog/product_view_options_type_text','catalog/product/view/options/type/text.phtml');
                    $options_block->addOptionRenderer('file','catalog/product_view_options_type_file','catalog/product/view/options/type/file.phtml');
                    $options_block->addOptionRenderer('date','catalog/product_view_options_type_date','catalog/product/view/options/type/date
                        .phtml');

                    $wrapper_block->setChild("option_js",$option_js_block);
                    $wrapper_block->setChild("option_wrapper",$options_block);

                }
                $configurable_block=$layout->createBlock('catalog/product_view_type_configurable')->setTemplate('ajaxcart/catalog/product/view/type/options/configurable.phtml');

                $wrapper_block->setChild("options_configurable",$configurable_block);

                $finalHtml['custom_option']=$wrapper_block->toHtml();

                $price_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/price_clone.phtml');
                $finalHtml['price']=$price_block->toHtml();

                $add_To_cart_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/addtocart.phtml');
                $finalHtml['add_to_cart']= $add_To_cart_block->toHtml();
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;

        }else if($product->getTypeId() == 'grouped'){
                $finalHtml['has_option']=1;
                $grouped_block = $layout->createBlock('catalog/product_view_type_grouped')->setTemplate('catalog/product/view/type/grouped.phtml');

                $finalHtml['custom_option']=$grouped_block->toHtml();

                $price_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/price_clone.phtml');
                $finalHtml['price']=$price_block->toHtml();

                $add_To_cart_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/addtocart.phtml');
                $finalHtml['add_to_cart']= $add_To_cart_block->toHtml();
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;
                
        }else if($product->getTypeId() == 'downloadable'){
            $finalHtml['has_option']=1;
            $wrapper_block = $layout->createBlock('catalog/product_view')->setTemplate('ajaxcart/catalog/product/view/options/wrapper.phtml');
            if($product->getHasOptions())
                {
                    $option_js_block = $layout->createBlock('core/template')->setTemplate('catalog/product/view/options/js.phtml');

                    $options_block = $layout->createBlock('catalog/product_view_options',"product.info.options")->setTemplate('catalog/product/view/options.phtml');

                    $options_block->addOptionRenderer('select','catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml');
                    $options_block->addOptionRenderer('text','catalog/product_view_options_type_text','catalog/product/view/options/type/text.phtml');
                    $options_block->addOptionRenderer('file','catalog/product_view_options_type_file','catalog/product/view/options/type/file.phtml');
                    $options_block->addOptionRenderer('date','catalog/product_view_options_type_date','catalog/product/view/options/type/date
                        .phtml');

                    $wrapper_block->setChild("option_js",$option_js_block);
                    $wrapper_block->setChild("option_wrapper",$options_block);

                }
                $downloadable_block= $layout->createBlock('downloadable/catalog_product_links')->setTemplate('downloadable/catalog/product/links.phtml');
                $wrapper_block->setChild("downloadable_wrapper",$downloadable_block);

                $finalHtml['custom_option']=$wrapper_block->toHtml();

                $price_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/price_clone.phtml');
                $finalHtml['price']=$price_block->toHtml();

                $add_To_cart_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/addtocart.phtml');
                $finalHtml['add_to_cart']= $add_To_cart_block->toHtml();
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;

        }else if($product->getTypeId()=='bundle'){
            $finalHtml['has_option']=1;
            $wrapper_block = $layout->createBlock('catalog/product_view')->setTemplate('ajaxcart/catalog/product/view/options/wrapper.phtml');
            if($product->getHasOptions())
                {
                    $option_js_block = $layout->createBlock('core/template')->setTemplate('catalog/product/view/options/js.phtml');

                    $options_block = $layout->createBlock('catalog/product_view_options',"product.info.options")->setTemplate('catalog/product/view/options.phtml');

                    $options_block->addOptionRenderer('select','catalog/product_view_options_type_select','catalog/product/view/options/type/select.phtml');
                    $options_block->addOptionRenderer('text','catalog/product_view_options_type_text','catalog/product/view/options/type/text.phtml');
                    $options_block->addOptionRenderer('file','catalog/product_view_options_type_file','catalog/product/view/options/type/file.phtml');
                    $options_block->addOptionRenderer('date','catalog/product_view_options_type_date','catalog/product/view/options/type/date
                        .phtml');

                    $wrapper_block->setChild("option_js",$option_js_block);
                    $wrapper_block->setChild("option_wrapper",$options_block);

                }
                $bundle_option = $layout->createBlock('bundle/catalog_product_view_type_bundle')->setTemplate('bundle/catalog/product/view/type/bundle/options.phtml');

                $bundle_option->addRenderer('select','bundle/catalog_product_view_type_bundle_option_select');
                $bundle_option->addRenderer('multi','bundle/catalog_product_view_type_bundle_option_multi');
                $bundle_option->addRenderer('radio','bundle/catalog_product_view_type_bundle_option_radio');
                $bundle_option->addRenderer('checkbox','bundle/catalog_product_view_type_bundle_option_checkbox');

                $wrapper_block->setChild("bundle_wrapper",$bundle_option);

                $bundle_html = $layout->createBlock('bundle/catalog_product_view_type_bundle')->setTemplate('bundle/catalog/product/view/type/bundle.phtml');

                $wrapper_block->setChild("bundle_html",$bundle_html);

                $finalHtml['custom_option']=$wrapper_block->toHtml();

                // $price_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/price_clone.phtml');
                // $price_block->addPriceBlockType('bundle','bundle/catalog_product_price','bundle/catalog/product/view/price.phtml');

                $cofig_price=$layout->createBlock('bundle/catalog_product_price')->setTemplate('bundle/catalog/product/view/price.phtml');
                $cofig_price->setMAPTemplate('catalog/product/price_msrp_item.phtml');

                // $finalHtml['price']=$price_block->toHtml();
                $finalHtml['price']=$cofig_price->toHtml();
                $add_To_cart_block = $layout->createBlock('catalog/product_view')->setTemplate('catalog/product/view/addtocart.phtml');
                $finalHtml['add_to_cart']= $add_To_cart_block->toHtml();
                $finalHtml=json_encode($finalHtml);
                return $finalHtml;
        }


    }

	public function getJsonConfig($product_id)
    {
        $product = Mage::getModel('catalog/product')->load($product_id);
    	$config = array();

        if (!$product->getHasOptions()) {
            return Mage::helper('core')->jsonEncode($config);
        }

        $_request = Mage::getSingleton('tax/calculation')->getDefaultRateRequest();
        /* @var $product Mage_Catalog_Model_Product */
        $_request->setProductClassId($product->getTaxClassId());
        $defaultTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_request = Mage::getSingleton('tax/calculation')->getRateRequest();
        $_request->setProductClassId($product->getTaxClassId());
        $currentTax = Mage::getSingleton('tax/calculation')->getRate($_request);

        $_regularPrice = $product->getPrice();
        $_finalPrice = $product->getFinalPrice();
        if ($product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            $_priceInclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, true,
                null, null, null, null, null, false);
            $_priceExclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, false,
                null, null, null, null, null, false);
        } else {
            $_priceInclTax = Mage::helper('tax')->getPrice($product, $_finalPrice, true);
            $_priceExclTax = Mage::helper('tax')->getPrice($product, $_finalPrice);
        }
        $_tierPrices = array();
        $_tierPricesInclTax = array();
        foreach ($product->getTierPrice() as $tierPrice) {
            $_tierPrices[] = Mage::helper('core')->currency(
                Mage::helper('tax')->getPrice($product, (float)$tierPrice['website_price'], false) - $_priceExclTax
                    , false, false);
            $_tierPricesInclTax[] = Mage::helper('core')->currency(
                Mage::helper('tax')->getPrice($product, (float)$tierPrice['website_price'], true) - $_priceInclTax
                    , false, false);
        }
        $config = array(
            'productId'           => $product->getId(),
            'priceFormat'         => Mage::app()->getLocale()->getJsPriceFormat(),
            'includeTax'          => Mage::helper('tax')->priceIncludesTax() ? 'true' : 'false',
            'showIncludeTax'      => Mage::helper('tax')->displayPriceIncludingTax(),
            'showBothPrices'      => Mage::helper('tax')->displayBothPrices(),
            'productPrice'        => Mage::helper('core')->currency($_finalPrice, false, false),
            'productOldPrice'     => Mage::helper('core')->currency($_regularPrice, false, false),
            'priceInclTax'        => Mage::helper('core')->currency($_priceInclTax, false, false),
            'priceExclTax'        => Mage::helper('core')->currency($_priceExclTax, false, false),
            /**
             * @var skipCalculate
             * @deprecated after 1.5.1.0
             */
            'skipCalculate'       => ($_priceExclTax != $_priceInclTax ? 0 : 1),
            'defaultTax'          => $defaultTax,
            'currentTax'          => $currentTax,
            'idSuffix'            => '_clone',
            'oldPlusDisposition'  => 0,
            'plusDisposition'     => 0,
            'plusDispositionTax'  => 0,
            'oldMinusDisposition' => 0,
            'minusDisposition'    => 0,
            'tierPrices'          => $_tierPrices,
            'tierPricesInclTax'   => $_tierPricesInclTax,
        );

        $responseObject = new Varien_Object();
        Mage::dispatchEvent('catalog_product_view_config', array('response_object' => $responseObject));
        if (is_array($responseObject->getAdditionalOptions())) {
            foreach ($responseObject->getAdditionalOptions() as $option => $value) {
                $config[$option] = $value;
            }
        }

        return Mage::helper('core')->jsonEncode($config);
    }


}

   