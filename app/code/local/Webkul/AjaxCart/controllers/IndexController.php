<?php
require_once 'Mage/Catalog/controllers/ProductController.php';
class Webkul_AjaxCart_IndexController extends Mage_Core_Controller_Front_Action
{
	public function productdetailAction()
	{
		$product_id=$this->getRequest()->getParam('product_id');
		$product = Mage::getModel('catalog/product')->load($product_id);
		$finalHtml=Mage::getModel('ajaxcart/productblock')->getAllBlocks($product_id);
		$this->getResponse()->setHeader('Content-type', 'application/json');
		$this->getResponse()->setBody($finalHtml);
	}
}