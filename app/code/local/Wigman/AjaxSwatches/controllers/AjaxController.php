<?php

class Wigman_AjaxSwatches_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function updateAction()
    {
        if (!isset($_REQUEST['pid'])) {
            exit;
        }

        $pId = $_REQUEST['pid'];
        $isSimpleProductRequested = $_REQUEST['isSimpleProductSelected'];

        if ($isSimpleProductRequested == 'true') {
            // This means we have all options selected to be able to pull a simple product
            $this->returnSimpleProduct($pId);
        } else {
            // We need to find a configurable matching the variation
            $this->returnConfigurableProduct($pId);
        }
    }

    private function getConfigurableProduct($simpleProductId)
    {
        $correctProduct = null;
        if(!is_object($simpleProductId) && is_numeric($simpleProductId)) {
            $_product = Mage::getModel('catalog/product')
                ->load($simpleProductId);
        } else {
            $_product = $simpleProductId;
        }

        $variation = $this->getProductVariation($_product);

        if ($variation == null) {
            Mage::throwException('Variation not found for product id '.$simpleProductId);
        }

        // Find configurable product for this variation

        $configurable = Mage::getResourceModel('catalog/product_collection')
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('type_id', 'configurable')
        ->addAttributeToFilter('default_selected_variation',array('eq' => $variation));

        $correctProduct = $configurable->getFirstItem();

        if ($correctProduct == null) {
            Mage::throwException('Could not find the correct product for and variation: '.$variation.' based on Default Selected Variation. Is it set on your configurable?');
        }

        return $correctProduct;
    }

    private function returnConfigurableProduct($pId)
    {
        $correctProduct = $this->getConfigurableProduct($pId);
        $product =
        $_product = Mage::getModel('catalog/product')
        ->load($correctProduct->getId());

        $this->sendDetails($_product, $_product->getMediaGalleryImages());
    }

    private function getProductVariation($_product, $labelToLookFor = 'Variation')
    {
        $variation = null;
        $attributes = $_product->getAttributes();

        foreach ($attributes as $attribute) {
            //$attributeCode = $attribute->getAttributeCode();  //Unused var....
            if ($attribute->getId() == null) { //Some ids are null, who knows why....
                continue;
            }
            $label = $attribute->getStoreLabel();
//            Mage::log("attrib loading:" . strval($attribute->getId()) . " - " . $label, null, 'mylogfile.log');
            if ($label == $labelToLookFor) {
                $variation = $attribute->getFrontend()->getValue($_product);
                break;
            }
        }

        return $variation;
    }

    private function sendDetails($_product, $mediaImages = null, $useParentImages = false)
    {
        $images = [];

        if ($mediaImages) {
            $detailImg1 = $mediaImages->getItemByColumnValue('label', 'detail-1');
            $detailImg2 = $mediaImages->getItemByColumnValue('label', 'detail-2');

            $images = array(
                'detail1' => $detailImg1 ? $detailImg1->getUrl() : null,
                'detail2' => $detailImg2 ? $detailImg2->getUrl() : null,
            );
        }

        $isSaleable = false;

        /*$cart = Mage::getModel('checkout/session')->getQuote();
        $cartItemQuantity = 0;

        foreach ($cart->getAllItems() as $item) {
            if ($item->getParentItemId() || $item->getOptionByCode('simple_product')->getProduct()->getId() != $_product->getId()) {
                continue;
            }

            $cartItemQuantity = $item->getQty();
        }

        if ($_product->isSaleable()) {
            if ((int) $_product->getStockItem()->getQty() > $cartItemQuantity) {
                $isSaleable = true;
            }
        }*/

        $formUrl = $_product->getTypeId() == 'simple' ? Mage::helper('checkout/cart')->getAddUrl($this->getConfigurableProduct($_product)) : Mage::helper('checkout/cart')->getAddUrl($_product);

        $response = [
	        'description' => $_product->getDescription(),
	        'short_description' => $_product->getShortDescription(),
	        'short_spec' => $_product->getShortSpec(),
	        'images' => $images,
	        'productCareInfo' => $this->getProductCareInfo($_product),
	        'relatedProducts' => $this->getRelatedProductsHTML($_product),
	        'formUrl' => $formUrl,
	        'extraText' => $_product->getExtraText(),
	        'isPreOrder' => $_product->getIsPreOrder() ? true : false,
	        'title' => $_product->getName(),
	        'url' => $_product->getProductUrl()
        ];

	    if( $useParentImages ) {
		    $_product = $this->getConfigurableProduct($_product->getId());
	    }

	    $response['thumbnailHtmlId'] = $_product->getId();
	    $response['thumbnailHtml'] = $this->getThumbnailHtml($_product);
	    $response['mainImageHtmlId'] = $_product->getId();
	    $response['mainImageHtml'] = $this->getLayout()->createBlock( 'catalog/product_view_media',
		    'media',
		    array(
			    'template' => 'catalog/product/view/media.phtml',
			    'product'  => Mage::getModel( 'catalog/product' )->load( $_product->getId() )
		    )
	    )->toHtml();

        $this->getResponse()->clearHeaders()->setHeader('Content-type', 'application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));

        return;
    }

    private function getProductCareInfo($_product)
    {
        if ($_product->getTypeId() == 'simple') {
            $_product = $this->getConfigurableProduct($_product->getId());
        }

        if ($_product == null) {
            return false;
        }

        return  $_product->getProductCareInfo();
    }

    private function getRelatedProductsHTML($_product)
    {
        if ($_product->getTypeId() == 'simple') {
            $_product = $this->getConfigurableProduct($_product->getId());
        }

        if ($_product == null) {
            return false;
        }

        $related_product_collection = $_product->getRelatedProductCollection()
        ->addAttributeToSelect('required_options')
        ->addAttributeToSelect('image')
        ->setOrder('position', Varien_Db_Select::SQL_ASC)
        ->addStoreFilter();

        $retVal = ['<div class="multiple-items-carousel">'];

        foreach ($related_product_collection as $_item) {
            $_item = Mage::getModel('catalog/product')->load($_item->getId());
            $img = Mage::helper('catalog/image')->init($_item, 'small_image');
            $name = Mage::helper('core')->escapeHtml($_item->getName());
            $url = $_item->getProductUrl();

            $retVal[] = <<<EOF
			<div>
				<a href="$url" >
					<img src="$img" alt="$name">
				</a>
			</div>
EOF;
        }

        $retVal[] = '</div>';

        return implode('', $retVal);
    }

    private function returnSimpleProduct($pId)
    {
        $_product = Mage::getModel('catalog/product')
        ->load($pId);
        $this->sendDetails($_product, $_product->getMediaGalleryImages(), true);
    }

    private function getThumbnailHtml($_product) {
		$galleryImages = Mage::getModel('catalog/product')->load($_product->getId())->getMediaGalleryImages();
		$thumbnails = array();
		foreach ($galleryImages as $item) {
			if (strpos($item->getLabel(), 'gallery-thumb')===0) {
				array_push($thumbnails, $item);
			}
		}

		$html = '';
    	if(sizeof($thumbnails)) {
		    $hasArrows = ( sizeof( $thumbnails ) > 3 ) ? true : false;
		    if ( $hasArrows ) {
			    $html .= '<span class="top-arrow">&nbsp;</span>';
		    }

		    $html .= '<ul class="thumbnails-list" data-id="'.$_product->getEntityId.'">';
		    foreach ( $thumbnails as $thumb ):
			    $html .= '<li class="thumbnail" data-id="' . $thumb['product_id'] . '-' . $thumb['label'] . '">';
			    $html .= '<img width="80" height="80" src="' . $thumb['url'] . '" />';
			    $html .= '</li>';
		    endforeach;
		    $html .= '</ul>';

		    if ( $hasArrows ) {
			    $html .= '<span class="bottom-arrow">&nbsp;</span>';
		    }
	    }

	    return $html;
    }
}
