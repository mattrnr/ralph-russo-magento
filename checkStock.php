<?php
require_once('app/Mage.php');
try {
    Mage::init();

    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');

    $query = "SELECT `cataloginventory_stock_status`.`product_id`, `cataloginventory_stock_status`.`qty`, `cataloginventory_stock_status`.`stock_status`
 FROM `cataloginventory_stock_status` WHERE (product_id IN(:pid)) AND (stock_id=1) AND (website_id=1);";

    $pid = Mage::app()->getRequest()->getParam('pid');

    $binds = array('pid' => $pid);
    $result = $readConnection->fetchAll($query, $binds);

    if(count($result[0]) < 1) {
        throw new Exception('No stock found');
    }

    //Check against current basket level
    Mage::getSingleton("core/session", array("name" => "frontend"));
    $cart = Mage::getSingleton('checkout/session')->getQuote();
    $cartItemQuantity = 0;

    foreach ($cart->getAllItems() as $item) {
        if ($item->getParentItemId() || $item->getOptionByCode('simple_product')->getProduct()->getId() != $pid) {
            continue;
        }

        $cartItemQuantity = $item->getQty();
    }

    if ($result[0]['stock_status']) {
        if ((int) $result[0]['qty'] <= $cartItemQuantity) {
            $result[0]['stock_status'] = "0";
        }
    }

    echo json_encode(array_merge(array('success' => 'true','data' => $result[0], 'cartQty' => $cartItemQuantity)));
} catch (Exception $e) {
    echo $e->getMessage();
    echo json_encode(array('success' => 'false'));
}
