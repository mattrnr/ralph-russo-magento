<html>


<head>

	<script	src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

</head>

<style>
	img {
		max-width: 500px;
	}

	input {
		width: 100%;
		padding: 20px;
		font-size: 20px;
	}
</style>

<h4>Type in the	name of	the image</h4>

<input type="text" id="img-url"  placeholder="Image URL" />

<h2>S3</h2>
<img id="s3" />	

<h2>CDN</h2>
<img id="cdn">

<script>

	$("#img-url").on("change, keyup", function() {

		var s3Base = "https://s3-eu-west-1.amazonaws.com/ralph-and-russo-media-stage";
		var cdnBase = "https://dbl3m9kv9sfip.cloudfront.net";

		var imgUrl = $("#img-url").val();
		console.log(imgUrl[0])


		imgUrl = imgUrl.replace(s3Base, "");
		imgUrl = imgUrl.replace(cdnBase, "")

		if (imgUrl[0] != "/")
			imgUrl = "/" + imgUrl;


		$("#s3").attr("src", s3Base  + imgUrl);         
		$("#cdn").attr("src", cdnBase  + imgUrl);

	});

</script>
</html>

