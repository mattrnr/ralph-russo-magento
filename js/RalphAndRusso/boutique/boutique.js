window.ralphAndRusso = window.ralphAndRusso || {};
ralphAndRusso.boutique = ralphAndRusso.boutique || {}

jQuery(document).ready(function() {
    ralphAndRusso.boutique.init();
})

ralphAndRusso.boutique.init = function() {
    ralphAndRusso.boutique.setCarousel();

}

ralphAndRusso.boutique.setCarousel = function() {

    if(!jQuery('.multiple-items-carousel').hasClass('slick-initialized')) {
        jQuery('.multiple-items-carousel').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if(!jQuery('#carousels-boutique-carousel').hasClass('slick-initialized')) {
        jQuery('#carousels-boutique-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
}
