jQuery(window).ready(function() {
    /*Initializing Packery */

    jQuery('.grid-container').imagesLoaded(function() {
        // $('.grid-container').packery({
        //  'fit': '.grid',
        //  'transitionDuration': '0.0s',
        //  // "percentPosition": true,
        // });
        jQuery('.grid-container').isotope({
            itemSelector: '.grid',
            percentPosition: true,
            transitionDuration: 0,
            masonry: {
                columnWidth: '.grid-sizer'
            }
        });
    });

    if(!jQuery('.multiple-items-carousel').hasClass('slick-initialized')) {
        jQuery('.multiple-items-carousel').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });
    }

    if(!jQuery('#carousels-more-shoe-styles').hasClass('slick-initialized')) {
        jQuery('#carousels-more-shoe-styles').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });

    }

    // haute couture is currently using this file, remove the below when separated
    if(!jQuery('#carousels-backstage-carousel').hasClass('slick-initialized')) {
        jQuery('#carousels-backstage-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
            ]
        });
    }

    jQuery(document).on('click', '#category-view-all-products', function() {
        var width = jQuery('.multiple-items-carousel div.slick-current').css('width');
        jQuery('.multiple-items-carousel').slick('unslick').addClass('view-all');
        jQuery('.multiple-items-carousel > div').addClass('view-all-item').css('max-width', width);
        jQuery(this).hide();
    });



})
