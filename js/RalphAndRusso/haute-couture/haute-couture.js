jQuery(window).load(function() {
    /*Initializing Packery */

    jQuery('.grid-container').imagesLoaded(function() {
        // $('.grid-container').packery({
        //  'fit': '.grid',
        //  'transitionDuration': '0.0s',
        //  // "percentPosition": true,
        // });
        jQuery('.grid-container').isotope({
            itemSelector: '.grid',
            percentPosition: true,
            transitionDuration: 0,
            masonry: {
                columnWidth: '.grid-sizer'
            }
        });
    });

    if(!jQuery('.multiple-items-carousel').hasClass('slick-initialized')) {
        jQuery('.multiple-items-carousel').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if(!jQuery('#carousels-backstage-carousel').hasClass('slick-initialized')) {
        jQuery('#carousels-backstage-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }


})
