window.ralphAndRusso = window.ralphAndRusso || {};
ralphAndRusso.lookbook = ralphAndRusso.lookbook || {
    // the roller element that is a container for the large look
    rollerEl: null,
    lookHashRegEx: /\?look\=(\d*)/,
    noOfColsInRow: null,
    enquiryMessage: "I would like to enquire about %s, please provide further details."

}

jQuery(document).ready(function() {
    ralphAndRusso.lookbook.init();
    ralphAndRusso.lookbook.loadSelection();
    ralphAndRusso.lookbook.enableCloseBtn()

    ralphAndRusso.lookbook.noOfColsInRow = ralphAndRusso.lookbook.getNoOfColsInRow();
    jQuery(window).resize(ralphAndRusso.lookbook.onWindowResize);

    var offset = (new Date()).getTimezoneOffset();
    
    if ($("#geolocate")) {
        if (offset => 0){
            $("#geolocate").attr("value", "GMT +" + offset);
        } else {
            $("#geolocate").attr("value", "GMT " + offset);
        }     
    }

    jQuery('#make-an-enquiry-modal').on({
        'show.uk.modal': function(){
            jQuery("#bookAnEnquiryForm textarea[name=message]").val(ralphAndRusso.lookbook.enquiryMessage.replace("%s", jQuery("#fashion-season").text() + " " + jQuery("#lookbook-roller-detail-title").text()));
            
            var runway_type, season, look;
            
            if (jQuery("#fashion-season").text().includes("READY-TO-WEAR")) {
                runway_type = "ready_to_wear";
            } else {
                runway_type = "haute_couture_enquiry";
            }

            var seasonArray = {"Ralph & Russo Couture Spring Summer 2019": "collection_season_ss19", "Ralph & Russo Couture Autumn Winter 2018": "collection_season_aw18", "S/S 18": "collection_season_ss18", "A/W 17/18": "collection_season_aw17", "S/S 2017": "collection_season_ss17", "A/W 16/17": "collection_season_aw16", "S/S 2016": "collection_season_ss16", "A/W 15/16": "collection_season_aw15", "S/S 2015": "collection_season_ss15", "A/W 2014": "collection_season_aw14", "S/S 2014": "collection_season_ss14", "READY-TO-WEAR A/W 2018": "collection_season_aw18", "READY-TO-WEAR S/S 2018": "collection_season_ss18"};
            var replaceFrom = jQuery("#fashion-season").text();
            season = seasonArray[replaceFrom];

            look = jQuery("#lookbook-roller-detail-title").text().toLowerCase().replace("runway ", "").replace("l", "L");

            jQuery("#lookbook-runway-type").val(runway_type);
            jQuery("#lookbook-season").val(season);
            jQuery("#lookbook-look").val(look);
        }
    });

    // remove coutue
    jQuery(".breadcrumbs .category60").remove();
})

ralphAndRusso.lookbook.enableCloseBtn = function() {
    jQuery(".lookbook-roller-detail .close-btn").on("click", ralphAndRusso.lookbook.hideRoller);
}

ralphAndRusso.lookbook.onWindowResize = function() {

    var noOfColsInRow = ralphAndRusso.lookbook.getNoOfColsInRow();

    if (ralphAndRusso.lookbook.noOfColsInRow != noOfColsInRow) {
        ralphAndRusso.lookbook.rapidHideRoller();
    }

    ralphAndRusso.lookbook.noOfColsInRow = noOfColsInRow;


    if (ralphAndRusso.lookbook.isRollerVisible())
        ralphAndRusso.lookbook.setRollerHeight();
}

ralphAndRusso.lookbook.getNoOfColsInRow = function() {
    return Math.floor(jQuery("#lookbook-grid").width() / jQuery(".lookbook-pane").first().width());
}

ralphAndRusso.lookbook.loadSelection = function() {

    var hash = window.location.href;

    if (hash == null || ralphAndRusso.lookbook.lookHashRegEx.exec(hash) == null)
        return;

    var lookNo = ralphAndRusso.lookbook.lookHashRegEx.exec(hash)[1];
    // console.log("Attempting to open look " + lookNo);

    var pane = jQuery(".lookbook-pane[data-id=" + lookNo + "]");

    if (pane.length == 1) {
        setTimeout(function() {
            pane.trigger("click");
        }, 500)
    } else {
        console.log(".lookbook-pane[data-id=" + lookNo + "] returned != 1 results");
    }
}

ralphAndRusso.lookbook.init = function() {

    ralphAndRusso.lookbook.rollerEl = jQuery("#lookbook-roller");

    jQuery(window).resize(function() {
        if(Modernizr.mq('(max-width: 767px)')) {
            jQuery('.lookbook-pane').each(function(){
                jQuery(this).attr('data-uk-modal', "{target:'#lookbook-pane-modal', center:false}").addClass('mobile');
            });
        }
        else {
            jQuery('.lookbook-pane').each(function(){
                jQuery(this).removeAttr('data-uk-modal').removeClass('mobile');
            });
        }
    }).resize();

    jQuery('#lookbook-pane-modal').on({
        'hide.uk.modal': function(){
            jQuery('#lookbook-pane-modal .modal-content h4, #lookbook-pane-modal .modal-content h3, #lookbook-pane-modal .modal-content p#lookbook-roller-detail-description').html('');
        }
    });

    jQuery("#lookbook-grid").on("click", ".lookbook-pane:not(.mobile)", ralphAndRusso.lookbook.onPaneClick);

    jQuery("#lookbook-grid").on("click", ".lookbook-pane.mobile", ralphAndRusso.lookbook.onMobilePaneClick);

}

ralphAndRusso.lookbook.onPaneClick = function() {
    ralphAndRusso.lookbook.showLargeLook(this);

    jQuery(window).trigger("AT:PaneClick", this);
}

ralphAndRusso.lookbook.onMobilePaneClick = function() {
    ralphAndRusso.lookbook.showModal(this);

    jQuery(window).trigger("AT:PaneClickMobile", this);

}

ralphAndRusso.lookbook.showModal = function(el) {

    var image = jQuery('img', el).attr('src');
    var largeImgSrc = jQuery('img', el).attr("data-large-image");
    var largeImgWidth = jQuery('img', el).attr("data-large-image-width");
    var largeImgHeight = jQuery('img', el).attr("data-large-image-height");
    var description  = jQuery(el).attr("data-description") != null ? jQuery(el).attr("data-description") : "";
    var shortDescription = jQuery(el).attr("data-short-description") != null ? jQuery(el).attr("data-short-description") : "";
    var h4  = jQuery(el).attr("data-description") != null ? jQuery(el).attr("data-h4") : "";
    var title = jQuery(el).attr("data-name") != null ? jQuery(el).attr("data-name") : "";

    jQuery("#lookbook-pane-modal .modal-content p#lookbook-roller-detail-description").html(description);
    jQuery("#lookbook-pane-modal .modal-content p#lookbook-roller-detail-short-description").html(shortDescription);
    jQuery("#lookbook-pane-modal .modal-content #lookbook-roller-image").attr("src", image);
    jQuery("#lookbook-pane-modal .modal-content a.zoom-btn").attr("href", largeImgSrc);
    jQuery("#lookbook-pane-modal .modal-content a.zoom-btn").attr("data-size", largeImgWidth + 'x' + largeImgHeight);
    jQuery(" #lookbook-pane-modal .modal-content h3").html(title);
    jQuery(" #lookbook-pane-modal .modal-content h4").html(h4);

    initPhotoSwipeFromDOM('#lookbook-pane-modal .zoom-btn');

    ralphAndRusso.lookbook.updateMeta(el)

}


ralphAndRusso.lookbook.showLargeLook = function(el) {

    if (needToMoveRoller(el)) {
        if (ralphAndRusso.lookbook.rollerEl.hasClass("roller-visible")) {
            ralphAndRusso.lookbook.hideRoller();
            ralphAndRusso.lookbook.rollerEl.one("transitionend", function() {
                ralphAndRusso.lookbook.showLargeLook(el);
            })

        } else {

            var currentLook = ralphAndRusso.lookbook.selectedLook;

            ralphAndRusso.lookbook.positionRollerAboveThumb(el);
            ralphAndRusso.lookbook.markSelectedLook(el);

            if (currentLook != ralphAndRusso.lookbook.selectedLook) {
                setTimeout(function() {
                    ralphAndRusso.lookbook.showRoller();

                    jQuery('#footer').css({
                      position: 'relative'
                    });
                }, 500);
            } else {
                ralphAndRusso.lookbook.markSelectedLook(null);
            }
        }
    } else {
        // update the image on the visible lookbook
        ralphAndRusso.lookbook.markSelectedLook(el);
        ralphAndRusso.lookbook.scrollToView();
    }

    function needToMoveRoller(el) {
        return ralphAndRusso.lookbook.isRollerVisible() == false || ralphAndRusso.lookbook.rollerNeedsPositioning(el);
    }
}

ralphAndRusso.lookbook.updateMeta = function(el) {

    el = jQuery(el);

    var description  = el.attr("data-description") != null ? el.attr("data-description") : "";
    var shortDescription  = el.attr("data-short-description") != null ? el.attr("data-short-description") : "";
    var title =jQuery(el).attr("data-name") != null ? jQuery(el).attr("data-name") : "";
    var largeImgSrc = el.find("img").first().attr("data-large-image");
    var largeImgWidth = el.find("img").first().attr("data-large-image-width");
    var largeImgHeight = el.find("img").first().attr("data-large-image-height");
    var shortSpec = el.attr("data-short-spec") != null ? el.attr("data-short-spec") : "";
    jQuery("#lookbook-roller-detail-description").html(description);
    jQuery("#product-variation-name").html(shortDescription);
    jQuery("#lookbook-roller-image").attr("src", el.find("img").first().attr("src"));
    jQuery("#lookbook-content-image-container a").attr("href", el.find("img").first().attr("data-large-image"));
    jQuery("#lookbook-roller-detail-title").html(title);
    jQuery("#lookbook-roller-detail-short-spec").html(shortSpec);console.log(shortSpec);

    jQuery("#main > div > div.breadcrumbs > ul > li:last-child > strong").html(title);
    jQuery("#lookbook-content-image-container a.zoom-btn").attr("href", largeImgSrc);
    jQuery("#lookbook-content-image-container img.lightbox-large-image").attr("src", largeImgSrc);
    jQuery("#lookbook-content-image-container a.zoom-btn").attr("data-size", largeImgWidth + 'x' + largeImgHeight);

    var lightBox = jQuery("#lookbook-content-image-container a.zoom-btn").data("lightbox");
    jQuery.preloadImages(largeImgSrc);

    if (lightBox) {
        lightBox.data.item.source = el.find("img").first().attr("data-large-image");
        jQuery("#lookbook-content-image-container a.zoom-btn").data("lightbox", lightBox);
    }


    window.history.pushState("page", "PageTitle", window.location.pathname + "?look=" + el.attr("data-id"));

    jQuery(".social-media-links a.facebook").attr("href", "http://www.facebook.com/sharer.php?u=" + escape("https://ralphandrusso.com" + window.location.pathname) + "?look=" + el.attr("data-id") + "&p[title]=" + escape("Lookbook look " + el.attr("data-id")))

    jQuery("meta[property='og:image']").attr("content", el.find("img").attr("src"));
    jQuery("meta[property='og:description']").attr("content",el.attr("data-description"));

    jQuery(".social-media-links a.twitter").attr("href", "https://twitter.com/intent/tweet?url=" + escape(window.location.href) + "&text=" + el.attr("data-description"));

    jQuery(".social-media-links a.pinterest").attr("href", "//www.pinterest.com/pin/create/button/?url=" + escape(window.location.href) + "&media=" + el.find("img").attr("src") + "&description=" + el.attr("data-description") );

    jQuery(".social-media-links a.weibo").attr("href", "http://service.weibo.com/share/share.php?url=" + escape(window.location.href) + "&pic=" + el.find("img").attr("src") + "&title=" + el.attr("data-description"));

    jQuery(".social-media-links a.email").attr("href", "mailto:?subject=&body=" +  el.attr("data-description") + ": " + window.location.href);

}

ralphAndRusso.lookbook.markSelectedLook = function(el) {

    jQuery(".lookbook-pane.lookbook-selected-look").removeClass("lookbook-selected-look");

    if (el == null) {
        ralphAndRusso.lookbook.selectedLook = null;
        return;
    }

    el = jQuery(el);
    el.addClass("lookbook-selected-look");
    ralphAndRusso.lookbook.updateMeta(el);
    ralphAndRusso.lookbook.selectedLook = jQuery(el).attr("data-id");

    initPhotoSwipeFromDOM('#lookbook-content-image-container .zoom-btn');
}

ralphAndRusso.lookbook.scrollToView = function() {
    jQuery("html, body").animate({
        "scrollTop" : ralphAndRusso.lookbook.rollerEl.position().top
    })

}

ralphAndRusso.lookbook.rollerNeedsPositioning = function(el) {


    if (ralphAndRusso.lookbook.isRollerVisible() == false)
        return true;

    var currentLook = jQuery(".lookbook-selected-look").first();


    return jQuery(el).offset().top != currentLook.offset().top;
}

ralphAndRusso.lookbook.positionRollerAboveThumb = function(el) {

    el = jQuery(el);

    if (el.hasClass("lookbook-pane") == false)
        throw "Expected lookbook-pane, got " + jQuery(el).clone().wrap("<div />").html();


    // find the first element in this row to prepend
    var candidate = el;
    var found = false;
    var prev = null;
    var selectedPaneTop = el.offset().top;

    while(found == false) {

        prev = candidate.prev(".lookbook-pane");

        if (prev.length == 0) {
            found = true;
            break;
        }

        candidate = prev;

        if (candidate.offset().top < selectedPaneTop) {
            found = true;
            candidate = prev.next(".lookbook-pane")
        }

    }

    ralphAndRusso.lookbook.rollerEl.insertBefore(candidate);

}

ralphAndRusso.lookbook.isRollerVisible = function() {
    return ralphAndRusso.lookbook.rollerEl.hasClass("roller-visible");
}

ralphAndRusso.lookbook.hideRoller = function() {
    ralphAndRusso.lookbook.rollerEl.removeClass("roller-visible").addClass("roller-hidden");
    jQuery(".lookbook-pane.lookbook-selected-look").removeClass("lookbook-selected-look");
    ralphAndRusso.lookbook.markSelectedLook(null);
}


ralphAndRusso.lookbook.rapidHideRoller = function() {

    ralphAndRusso.lookbook.rollerEl.addClass("prevent-animation");
    ralphAndRusso.lookbook.hideRoller();

    ralphAndRusso.lookbook.rollerEl.insertBefore(jQuery(".lookbook-pane").first());
    ralphAndRusso.lookbook.rollerEl.removeClass("prevent-animation");
}


ralphAndRusso.lookbook.showRoller = function() {
    ralphAndRusso.lookbook.rollerEl.removeClass("roller-hidden").addClass("roller-visible");
    ralphAndRusso.lookbook.setRollerHeight();
    ralphAndRusso.lookbook.scrollToView();

}

ralphAndRusso.lookbook.setRollerHeight = function() {
    ralphAndRusso.lookbook.rollerEl.height(jQuery("#lookbook-roller-image").height())
    ralphAndRusso.lookbook.rollerEl.find(".uk-vertical-align").height(jQuery("#lookbook-roller-image").height())
}


var initPhotoSwipeFromDOM = function(gallerySelector) {

    // parse slide data (url, title, size ...) from DOM elements
    // (children of gallerySelector)
    var parseThumbnailElements = function(el) {
        var thumbElements = el.querySelectorAll('.lightbox-image'),
        numNodes = thumbElements.length,
        items = [],
        figureEl,
        linkEl,
        size,
        item;

        for(var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i]; // <figure> element

            // include only element nodes
            if(figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl; // <a> element

            size = linkEl.getAttribute('data-size').split('x');

            // create slide object
            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10)
            };



            if(figureEl.children.length > 1) {
                // <figcaption> content
                item.title = figureEl.children[1].innerHTML;
            }

            if(linkEl.children.length > 0) {
                // <img> thumbnail element, retrieving thumbnail url
                item.msrc = linkEl.children[0].getAttribute('src');
            }

            item.el = figureEl; // save link to element for getThumbBoundsFn
            items.push(item);
        }

        return items;
    };

    // find nearest parent element
    var closest = function closest(el, fn) {
        return el && ( fn(el) ? el : closest(el.parentNode, fn) );
    };

    // triggers when user clicks on thumbnail
    var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
            return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
        });

        if(!clickedListItem) {
            return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.parentNode,
        childNodes = clickedListItem.parentNode.childNodes,
        numChildNodes = childNodes.length,
        nodeIndex = 0,
        index;

        for (var i = 0; i < numChildNodes; i++) {
            if(childNodes[i].nodeType !== 1) {
                continue;
            }

            if(childNodes[i] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if(index >= 0) {
            // open PhotoSwipe if valid index found
            openPhotoSwipe( index, clickedGallery );
        }
        return false;
    };

    // parse picture index and gallery index from URL (#&pid=1&gid=2)
    var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
        params = {};

        if(hash.length < 5) {
            return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
            if(!vars[i]) {
                continue;
            }
            var pair = vars[i].split('=');
            if(pair.length < 2) {
                continue;
            }
            params[pair[0]] = pair[1];
        }

        if(params.gid) {
            params.gid = parseInt(params.gid, 10);
        }

        return params;
    };

    var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {

        var pswpElement = document.querySelectorAll('.pswp')[0],
        gallery,
        options,
        items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {

            // define gallery index (for URL)
            captionEl: true,
            fullscreenEl: false,
            zoomEl: false,
            shareEl: false,
            counterEl: false,
            arrowEl: false,
            preloaderEl: false,
            closeOnScroll: false,
            history: false,
            closeElClasses: ['close-btn'],
            barsSize: {top:0, bottom:0},
            scaleMode: 'fit-h',
            tapToToggleControls: false

        };

        // PhotoSwipe opened from URL
        if(fromURL) {
            if(options.galleryPIDs) {
                // parse real index when custom PIDs are used
                // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                for(var j = 0; j < items.length; j++) {
                    if(items[j].pid == index) {
                        options.index = j;
                        break;
                    }
                }
            } else {
                // in URL indexes start from 1
                options.index = parseInt(index, 10) - 1;
            }
        } else {
            options.index = parseInt(index, 10);
        }

        // exit if index not found
        if( isNaN(options.index) ) {
            return;
        }

        if(disableAnimation) {
            options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    // loop through all gallery elements and bind events
    var galleryElements = document.querySelectorAll( gallerySelector );

    for(var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i+1);
        galleryElements[i].onclick = onThumbnailsClick;
    }

    // Parse URL and open gallery if it contains #&pid=3&gid=1
    // var hashData = photoswipeParseHash();
    // if(hashData.pid && hashData.gid) {
    //     openPhotoSwipe( hashData.pid ,  galleryElements[ hashData.gid - 1 ], true, true );
    // }
};
