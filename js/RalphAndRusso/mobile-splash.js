jQuery(document).ready(function() {

    function _is_touch_device() {
        return 'ontouchstart' in window;
    };

    if (_is_touch_device()) {
        jQuery("#mobile-splash").show();

        setTimeout(function() {
            jQuery("#mobile-splash").fadeOut(500);
        }, 1600)
    };

})
