/**
 * Event.simulate(@element, eventName[, options]) -> Element
 *
 * - @element: element to fire event on
 * - eventName: name of event to fire (only MouseEvents and HTMLEvents interfaces are supported)
 * - options: optional object to fine-tune event properties - pointerX, pointerY, ctrlKey, etc.
 *
 *    $('foo').simulate('click'); // => fires "click" event on an element with id=foo
 *
 **/
(function(){
    var eventMatchers = {
        'HTMLEvents': /^(?:load|unload|abort|error|select|change|submit|reset|focus|blur|resize|scroll)$/,
        'MouseEvents': /^(?:click|dblclick|mouse(?:down|up|over|move|out))$/
    };
    var defaultOptions = {
        pointerX: 0,
        pointerY: 0,
        button: 0,
        ctrlKey: false,
        altKey: false,
        shiftKey: false,
        metaKey: false,
        bubbles: true,
        cancelable: true
    };

    Event.simulate = function(element, eventName) {
        var options = Object.extend(Object.clone(defaultOptions), arguments[2] || { });
        var oEvent, eventType = null;

        element = $(element);

        for (var name in eventMatchers) {
          if (eventMatchers[name].test(eventName)) { eventType = name; break; }
        }

        if (!eventType)
          throw new SyntaxError('Only HTMLEvents and MouseEvents interfaces are supported');

        if (document.createEvent) {
          oEvent = document.createEvent(eventType);
          if (eventType == 'HTMLEvents') {
            oEvent.initEvent(eventName, options.bubbles, options.cancelable);
        }
        else {
            oEvent.initMouseEvent(eventName, options.bubbles, options.cancelable, document.defaultView,
              options.button, options.pointerX, options.pointerY, options.pointerX, options.pointerY,
              options.ctrlKey, options.altKey, options.shiftKey, options.metaKey, options.button, element);
        }
        element.dispatchEvent(oEvent);
    }
    else {
      options.clientX = options.pointerX;
      options.clientY = options.pointerY;
      oEvent = Object.extend(document.createEventObject(), options);
      element.fireEvent('on' + eventName, oEvent);
    }
    return element;
    };

    Element.addMethods({ simulate: Event.simulate });
    }
)();

jQuery(window).ready(function() {


    jQuery("#detailed-info").on("click", "li",  function(e) {
        jQuery(this).toggleClass("active").siblings("#detailed-info li").removeClass("active");
        jQuery(".detailed-info-pane").eq(jQuery(this).index()).toggleClass("visible").siblings(".detailed-info-pane").removeClass("visible");

        adaptHeightOfDetailedInfo();
        e.stopPropagation();
    });

    setTimeout(function() {
        jQuery(".product-options option").each(function() {

            var el = jQuery(this);
            if (el.html().toLowerCase().replace(/\&amp;/g,'&') == defaultSelectedVariation) {
                el.prop("selected", "selected");
                // el.parents("select").trigger("change");
                var parentSelectId = el.parents("select").attr('id');
                $(parentSelectId).simulate('change');
                return false;
            }


        });


        jQuery(".configurable-swatch-list li img").each(function() {
            var el = jQuery(this);

            if (el.attr("title").toLowerCase() == defaultSelectedVariation ||
                el.attr("alt").toLowerCase() == defaultSelectedVariation) {
                el.parents("li").first().addClass("active");

            // do not return false as Slick creates more than one element
        }
    });
    }, 100);

    jQuery(document).on('DOMSubtreeModified', '.multiple-items-carousel', function(event) {
        var hasNavigationButtons = jQuery('.multiple-items-carousel button.slick-arrow').length > 0;

        if(hasNavigationButtons == true) {
            jQuery('.box-up-sell').addClass('carousel-with-navigation');
        } else {
            jQuery('.box-up-sell').removeClass('carousel-with-navigation');
        }
    });

    var multipleItemsCarousel = jQuery('#colour-options .multiple-items-carousel');
    if(multipleItemsCarousel.length > 0) {

        multipleItemsCarousel.slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            centerPadding: '30px',
            responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    centerPadding: '0px',
                    slidesToScroll: 2
                }
            }
            ]
        });

    }

    var variationsPickerCarousel = jQuery('#variations-picker .multiple-items-carousel');
    if (variationsPickerCarousel.length > 0) {

        variationsPickerCarousel.slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            centerPadding: '30px',
            prevArrow: jQuery('#variations-picker .slick-prev'),
            nextArrow: jQuery('#variations-picker .slick-next'),
            responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 2
                }
            }
            ]
        });
    }

    jQuery(document).on('click', '.configurable-swatch-list li a', function(e) {

        if(jQuery(this).parents('.size-select').length == 1 && jQuery(this).parents('.not-available').length == 1) {
            e.stopImmediatePropagation();
            e.stopPropagation();
            e.preventDefault();
            return false;
        }

        if(jQuery(this).parents('#variations-picker').length == 1) {
            jQuery(".configurable-swatch-list li").removeClass("active");
        }

        var selectId = jQuery(this).attr('data-select-id'),
        optionValue = jQuery(this).attr('data-option-value');
        jQuery('select#attribute' + selectId + ' option[value="' + optionValue + '"]').prop('selected', true);
        $('attribute' + selectId).simulate('change');

        if(jQuery(this).parents('#variations-picker').length == 1) {
            jQuery(this).parents("li").first().addClass("active");
        }

        if (jQuery(window).width() <= 768) {
            jQuery("html, body").animate({
                scrollTop: 0
            }, 400);
        }
    });


    setTimeout(function() {
        adjustProductDetailPosition();

        jQuery("#detailed-info li.detailed-nav-item:first").trigger('click');
        jQuery("#detailed-info div.uk-accordion.uk-visible-small h3:first").trigger('click');
    }, 500)

    jQuery(window).resize(function() {
        adjustProductDetailPosition();
    }).trigger("resize");


    jQuery(window).resize(function() {
        adjustHeaderSize();
        adaptHeightOfDetailedInfo();
    })

    setInterval(function() {
        adjustHeaderSize();
        adjustProductDetailPosition();
    }, 250);

    var allMultipleItemsCarousel = jQuery('.multiple-items-carousel');
    if (allMultipleItemsCarousel.length > 0) {
        if(!allMultipleItemsCarousel.hasClass('slick-initialized')) {
            allMultipleItemsCarousel.slick({
                infinite: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                centerPadding: '30px',
                responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        centerPadding: '0px',
                        slidesToScroll: 2
                    }
                }
                ]
            });
        }

    }

    var thumbnailsListLi = jQuery(".thumbnails-list li");
    var thumbnailsList = jQuery('.thumbnails-list');
    if (thumbnailsListLi.length) {
        if (thumbnailsListLi.length<2) {
            thumbnailsListLi.on('click', function () {
                var i = jQuery(this).data('slick-index'),
                    slick = jQuery('.thumbnails-list').slick('getSlick');
                showMainImageAfterClickOnThumbnail(slick, i);
            });
        } else {
            thumbnailsList.on('afterChange', function (event, slick, i) {
                showMainImageAfterClickOnThumbnail(slick, i);
            });
        }

        thumbnailsList.slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            vertical: true,
            prevArrow: jQuery('.top-arrow'),
            nextArrow: jQuery('.bottom-arrow'),
            asNavFor: '.product-image-gallery.mobile .slider',
            // centerMode: true,
            focusOnSelect: true
        });

        jQuery(".thumbnails-list li.slick-current").click();
        thumbnailsList.slick('slickGoTo', 0, true);
    }

    jQuery('.product-options .super-attribute-select').change(function(e) {
        var pid = updateMeta(this);
        //Recheck for all selected options
        if(!pid) {
            if(spConfig.getIdOfSelectedProduct()) {
                pid = spConfig.getIdOfSelectedProduct();
            }
        }
        if(pid) {
            var prodData = spConfig['config']['rrData'][pid];

            //Mark unavailable and check stock here
            jQuery.ajax({
                url: '/checkStock.php',
                dataType: 'json',
                type: 'get',
                data: {"pid": pid},
                success: function (data) {
                    if (data.success) {
                        jQuery("body").toggleClass("out-of-stock", jQuery("#product_addtocart_form .configurable-swatch-list li:not(.not-available)").length == 0);
                        jQuery("body").toggleClass("out-of-stock", data.data.stock_status === "0");
                        if(data.data.stock_status === "1") {
                            jQuery("#product-addtocart-button").removeAttr("disabled");
                        }
                    }
                }
            });
            //Update delivery time, description, short description from product settings
            var addToCartButtonText = prodData.is_pre_order ? 'PRE-ORDER' : 'ADD TO BAG';
            jQuery("#product-addtocart-button").attr('title', addToCartButtonText);
            jQuery("#product-addtocart-button span").text(addToCartButtonText);
            jQuery("#more-info-description").html(prodData.description);
            jQuery("#detailed-info-Details").html('<div class="uk-modal-dialog"><a class="close-btn uk-modal-close"></a>' +  '<h3>Details</h3>' + prodData.short_description + '</div>');
            var extraText = prodData.extra_text ? prodData.extra_text : defaultDeliveryTime;
            jQuery("#delivery-time").html(extraText).show();
            jQuery("#product_addtocart_form").attr("action", prodData.formUrl);
        }
    });
});

function adaptHeightOfDetailedInfo() {
  jQuery("#detailed-info").height(jQuery(".detailed-info-pane.visible").outerHeight(true) + jQuery('#detailed-info .box-collateral>li').outerHeight(true));
}


function adjustHeaderSize() {

    var shoe = jQuery("#product-main-image > div > div > img.gallery-image.visible")
        isStyleshot = shoe.hasClass('styleshot');

    if (shoe.length == 0) {
        return;
    }

    var shoeMaxHeight = 450;
    var shoeHeight = shoe.height();
    var value = (shoeMaxHeight - shoeHeight)
    var newHeight = shoeMaxHeight - value;
    var extraHeight = (isStyleshot?0:40);

    if(!jQuery('body').hasClass('category-eden-scarf')) {
        if (shoeHeight)
            jQuery("#main").css("background-position-y", -value);
        newHeight = shoeMaxHeight - value + extraHeight;
    }
    // ensure 25 pixels from the top is visible main-image-container
    jQuery("#product-single-header").height(newHeight)
}

function adjustProductDetailPosition() {
    var productSingleHeader = jQuery("#product-single-header .header-details");
    marginTop = (((jQuery("#product-single-header").height() - productSingleHeader.outerHeight()) / 2));
    productSingleHeader.css({"margin-top": marginTop, "opacity": 1});
}