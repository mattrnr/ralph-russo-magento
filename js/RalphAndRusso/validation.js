Validation.addMethods({
    validateElement : function(elm) {
        var result = false;
        var useTitles = this.options.useTitles;
        var callback = this.options.onElementValidate;
        resultFunction = function(elm) {
            if (elm.hasClassName('local-validation') && !this.isElementInForm(elm, this.form)) {
                return true;
            }
            return Validation.validate(elm,{useTitle : useTitles, onElementValidate : callback});
        };
        result = resultFunction(elm);
        this.options.onFormValidate(result, this.form);
        return result;
    }
});
