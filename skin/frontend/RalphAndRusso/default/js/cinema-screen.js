window.rr = window.rr || {}

window.rr.cinemaScreen = {}


jQuery(window).load(function() {
    jQuery(".cinema-screen").each(function() {

        if (jQuery(this).attr("data-no-resize")) {
            jQuery(this).height(jQuery(this).find("img").height())}
        })
    })

    jQuery(window).ready(function() {

        jQuery(".cinema-screen").on("click", rr.cinemaScreen.open);

        if (jQuery(".cinema-screen").length > 0) {
            jQuery(window).resize(function() {
                jQuery(".cinema-screen.open").each(function() {
                    var container = jQuery(this);
                    var height = null;
                    if (container.find("iframe").attr("src") != null) {
                        height = container.find("iframe").height()
                    } else {
                        height = container.find("img").height()
                    }

                    if (jQuery(this).attr("data-height")) {
                        height = jQuery(this).find("img").attr("data-height");
                    }

                    if (container.attr("data-no-resize") != null) {
                        container.find("iframe").height("100%")
                    } else {
                        container.height(height);
                    }


                })

            }).trigger("resize")
        }
    })

    rr.cinemaScreen.open = function() {
        var el = jQuery(this);
        var iframe = el.find("iframe");
        var img = el.find("img");

        el.removeAttr("style")
        el.addClass("open");

        iframe.attr("src", iframe.attr("data-src"))


        if (el.attr("data-no-resize") != null) {
            iframe.height(img.height());
        } else {
            var height = el.attr("data-height") != null ? el.attr("data-height") : 685;
            el.height(height);
        }

        jQuery(window).trigger("resize")
        iframe.on("load", function() {
            el.addClass("hide-image");
            iframe.fadeIn(600);
            img.fadeOut(600);

            jQuery(window).resize();

        })
    }
