/**
* Magento
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE_AFL.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@magento.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade Magento to newer
* versions in the future. If you wish to customize Magento for your
* needs please refer to http://www.magento.com for more information.
*
* @category    design
* @package     rwd_default
* @copyright   Copyright (c) 2006-2015 X.commerce, Inc. (http://www.magento.com)
* @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/
var optionsWired = false
var ConfigurableMediaImages = {
    imageType: null,
    productImages: {},
    imageObjects: {},

    arrayIntersect: function(a, b) {
        var ai=0, bi=0;
        var result = new Array();

        while( ai < a.length && bi < b.length )
        {
            if      (a[ai] < b[bi] ){ ai++; }
            else if (a[ai] > b[bi] ){ bi++; }
            else /* they're equal */
            {
                result.push(a[ai]);
                ai++;
                bi++;
            }
        }

        return result;
    },

    getCompatibleProductImages: function(productFallback, selectedLabels) {
        //find compatible products
        var compatibleProducts = [];
        var compatibleProductSets = [];
        selectedLabels.each(function(selectedLabel) {
            if(!productFallback['option_labels'][selectedLabel]) {
                return;
            }

            var optionProducts = productFallback['option_labels'][selectedLabel]['products'];
            compatibleProductSets.push(optionProducts);

            //optimistically push all products
            optionProducts.each(function(productId) {
                compatibleProducts.push(productId);
            });
        });

        //intersect compatible products
        compatibleProductSets.each(function(productSet) {
            compatibleProducts = ConfigurableMediaImages.arrayIntersect(compatibleProducts, productSet);
        });

        return compatibleProducts;
    },

    isValidImage: function(fallbackImageUrl) {
        if(!fallbackImageUrl) {
            return false;
        }

        return true;
    },

    getSwatchImage: function(productId, optionLabel, selectedLabels) {
        var fallback = ConfigurableMediaImages.productImages[productId];
        if(!fallback) {
            return null;
        }

        //first, try to get label-matching image on config product for this option's label
        var currentLabelImage = fallback['option_labels'][optionLabel];
        if(currentLabelImage && fallback['option_labels'][optionLabel]['configurable_product'][ConfigurableMediaImages.imageType]) {
            //found label image on configurable product
            return fallback['option_labels'][optionLabel]['configurable_product'][ConfigurableMediaImages.imageType];
        }

        var compatibleProducts = ConfigurableMediaImages.getCompatibleProductImages(fallback, selectedLabels);

        if(compatibleProducts.length == 0) { //no compatible products
            return null; //bail
        }

        //second, get any product which is compatible with currently selected option(s)
        $j.each(fallback['option_labels'], function(key, value) {
            var image = value['configurable_product'][ConfigurableMediaImages.imageType];
            var products = value['products'];

            if(image) { //configurable product has image in the first place
                //if intersection between compatible products and this label's products, we found a match
                var isCompatibleProduct = ConfigurableMediaImages.arrayIntersect(products, compatibleProducts).length > 0;
                if(isCompatibleProduct) {
                    return image;
                }
            }
        });

        //third, get image off of child product which is compatible
        var childSwatchImage = null;
        var childProductImages = fallback[ConfigurableMediaImages.imageType];
        compatibleProducts.each(function(productId) {
            if(childProductImages[productId] && ConfigurableMediaImages.isValidImage(childProductImages[productId])) {
                childSwatchImage = childProductImages[productId];
                return false; //break "loop"
            }
        });
        if (childSwatchImage) {
            return childSwatchImage;
        }

        //fourth, get base image off parent product
        if (childProductImages[productId] && ConfigurableMediaImages.isValidImage(childProductImages[productId])) {
            return childProductImages[productId];
        }

        //no fallback image found
        return null;
    },

    getImageObject: function(productId, imageUrl) {
        var key = productId+'-'+imageUrl;
        if(!ConfigurableMediaImages.imageObjects[key]) {
            var image = $j('<img />');
            image.attr('src', imageUrl);
            ConfigurableMediaImages.imageObjects[key] = image;
        }
        return ConfigurableMediaImages.imageObjects[key];
    },

    updateImage: function(el) {

        var select = $j(el);
        var label = select.find('option:selected').attr('data-label');
        var productId = optionsPrice.productId; //get product ID from options price object

        // do not take into account shoe size
        // if (select.attr("id") == 'attribute186') {
        //     return;
        // }

        //find all selected labels
        var selectedLabels = new Array();

        $j('.product-options .super-attribute-select').each(function() {
            var $option = $j(this);
            if($option.val() != '') {
                selectedLabels.push($option.find('option:selected').attr('data-label'));
            }
        });

        var swatchImageUrl = ConfigurableMediaImages.getSwatchImage(productId, label, selectedLabels);


        if(!ConfigurableMediaImages.isValidImage(swatchImageUrl)) {
            return;
        }

        var swatchImage = ConfigurableMediaImages.getImageObject(productId, swatchImageUrl);
        ProductMediaManager.swapImage(swatchImage);
    },

    wireOptions: function() {

        if (!optionsWired) {
            optionsWired = true;
        }
    },

    swapListImage: function(productId, imageObject) {
        var originalImage = $j('#product-collection-image-' + productId);


        if(imageObject[0].complete) { //swap image immediately

            //remove old image
            originalImage.addClass('hidden');
            $j('.product-collection-image-' + productId).remove();

            //add new image
            imageObject.insertAfter(originalImage);

        } else { //need to load image

            var wrapper = originalImage.parent();

            //add spinner
            wrapper.addClass('loading');

            //wait until image is loaded
            imagesLoaded(imageObject, function() {
                //remove spinner
                wrapper.removeClass('loading');

                //remove old image
                originalImage.addClass('hidden');
                $j('.product-collection-image-' + productId).remove();

                //add new image
                imageObject.insertAfter(originalImage);
            });

        }
    },

    swapListImageByOption: function(productId, optionLabel) {
        var swatchImageUrl = ConfigurableMediaImages.getSwatchImage(productId, optionLabel, [optionLabel]);
        if(!swatchImageUrl) {
            return;
        }

        var newImage = ConfigurableMediaImages.getImageObject(productId, swatchImageUrl);
        newImage.addClass('product-collection-image-' + productId);

        ConfigurableMediaImages.swapListImage(productId, newImage);
    },

    setImageFallback: function(productId, imageFallback) {
        ConfigurableMediaImages.productImages[productId] = imageFallback;
    },

    init: function(imageType) {
        ConfigurableMediaImages.imageType = imageType;
        ConfigurableMediaImages.wireOptions();
    }
};

function updateMeta(el) {

    var select = $j(el),
        label = select.find('option:selected').attr('data-label'),
        productId = optionsPrice.productId,
        updateImage = true;


    // don't respond to size -- this does not change the metadata
    if (select.attr("id") == "attribute186")  {
        return false;
    }


    if(typeof label != 'undefined') {
        var pid = spConfig.getIdOfSelectedProduct() || null;

        if(pid) {
            callAjaxtoGetProductMeta(pid, true, updateImage);
            return pid;
        } else {
            console.warn("No pId found. Querying to update media");

            pid = ConfigurableMediaImages.productImages[productId].option_labels[label].products[0];
            callAjaxtoGetProductMeta(pid, false);
        }
    }
};


// This function will ensure both variation and size are selected
function allMandatoryFieldsSelected() {
    var valid = true;
    jQuery(".product-options select").each(function() {

        var select = jQuery(this);

        if (select.val() == null || select.val() == "") {
            valid = false;
            return;
        }

    })

    return valid;

}

function showMainImageAfterClickOnThumbnail(slick, i) {
    console.log('.thumbnails-list - slick goes to' + i);
    var li = $j(slick.$slides[i]),
        newMainImageId = li.data('id').replace('-thumb', '');

    $j('.product-image-gallery .gallery-image').removeClass('visible');
    $j('#'+newMainImageId).addClass('visible');
}

function callAjaxtoGetProductMeta(productId, isSimpleProductSelected, image) {

    image = (image == null) ? true : image;

    if(jQuery("#product_addtocart_form .configurable-swatch-list li").length > 0) {
        jQuery("body").toggleClass("out-of-stock", jQuery("#product_addtocart_form .configurable-swatch-list li:not(.not-available)").length == 0);
    }

    $j.ajax({
        url: '/ajaxswatches/ajax/update',
        dataType: 'json',
        type : 'post',
        data: {"pid" : productId, "isSimpleProductSelected" : isSimpleProductSelected},
        success: function(data){


            if(data) {
                // the swatches should tell us if we have any option in stok
                jQuery("#product_addtocart_form").attr("action", data.formUrl);
                var url = data.url;
                var description = data.short_description || "";
                var title = data.title || "";

                if(!isSimpleProductSelected) {
                    var extraText = data.extraText ? data.extraText : defaultDeliveryTime;
                    jQuery("#delivery-time").html(extraText).show();

                    var addToCartButtonText = data.isPreOrder ? 'PRE-ORDER' : 'ADD TO BAG';
                    jQuery("#product-addtocart-button").attr('title', addToCartButtonText);
                    jQuery("#product-addtocart-button span").text(addToCartButtonText);
                }

                if (image) {
                    if (data.images.detail1) {
                        $j("#more-info-detail-1").attr("src", data.images.detail1).addClass("visible");
                        $j("#product-single-header .social-media-links.share a.pinterest").attr("data-pin-href", "//www.pinterest.com/pin/create/button/?url=" + url + "&media=" + data.images.detail1 + "&description=" + description);
                        $j("#product-single-header .social-media-links.share a.facebook").attr("href", "http://www.facebook.com/sharer.php?u=" + url + "&&p[title]=" + encodeURIComponent("Ralph & Russo - " + title));

                        $j('meta[property="og:image"]').attr("content", data.images.detail1);
                    }

                    $j('#product-single-header .short-spec').html(data.short_spec);

                    if (data.relatedProducts != false) {

                        if (data.relatedProducts == null) {
                            $j("#colour-options").hide()
                        } else {

                            $j("#colour-options .multiple-items-carousel").remove();
                            $j("#colour-options").append(data.relatedProducts).show();
                            jQuery("#colour-options .multiple-items-carousel").slick({
                                infinite: true,
                                slidesToShow: 4,
                                slidesToScroll: 1,
                                centerPadding: '30px',
                                responsive: [
                                    {
                                        breakpoint: 768,
                                        settings: {
                                            slidesToShow: 2,
                                            centerPadding: '0px',
                                            slidesToScroll: 2
                                        }
                                    }
                                ]
                            });

                        }
                    }

                    if ($j('#product-main-image > .product-image').data('id')!=data.mainImageHtmlId) {
                        $j('#product-main-image').html(data.mainImageHtml);
                    }

                    if ($j('#product-single-header .product-image-thumbnails > ul').data('id')!=data.thumbnailHtmlId) {
                        $j('#product-single-header .product-image-thumbnails').html(data.thumbnailHtml);
                        if ($j(".thumbnails-list li").length) {
                            if ($j(".thumbnails-list li").length < 2) {
                                $j('.thumbnails-list li').on('click', function () {
                                    var i = $j(this).data('slick-index'),
                                        slick = $j('.thumbnails-list').slick('getSlick');
                                    showMainImageAfterClickOnThumbnail(slick, i);
                                });
                            } else {
                                $j('.thumbnails-list').on('afterChange', function (event, slick, i) {
                                    showMainImageAfterClickOnThumbnail(slick, i);
                                });
                            }

                            $j('.thumbnails-list').slick({
                                infinite: true,
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                vertical: true,
                                prevArrow: $j('.top-arrow'),
                                nextArrow: $j('.bottom-arrow'),
                                asNavFor: '.product-image-gallery.mobile .slider',
                                // centerMode: true,
                                focusOnSelect: true
                            });
                        }

                        if (data.images.detail2) {
                            $j("#more-info-detail-2").attr("src", data.images.detail2).addClass("visible");

                            $j("#product-single-header .social-media-links.share a.pinterest").attr("data-pin-href", "//www.pinterest.com/pin/create/button/?url=" + url + "&media=" + data.images.detail2 + "&description=" + description);
                            $j("#product-single-header .social-media-links.share a.facebook").attr("href", "http://www.facebook.com/sharer.php?u=" + url + "&&p[title]=" + encodeURIComponent("Ralph & Russo - " + title));

                            $j('meta[property="og:image"]').attr("content", data.images.detail2);
                        }

                        $j(".thumbnails-list li.slick-current").click();
                        setTimeout(function () {
                            $j(".thumbnails-list").slick('setPosition');
                        }, 500)
                    }
                }

                if (data.productCareInfo) {
                    jQuery("#detailed-info-Product-Care").html('<div class="uk-modal-dialog"><a class="close-btn uk-modal-close"></a>' + '<h3>Product care</h3>' +  data.productCareInfo + '</div>');
                    adaptHeightOfDetailedInfo()
                }
            } else {
                return false;
            }
        }
    });
}

jQuery(window).on("popstate", function (e) {

    // Watch out with this funciton -- caused indeterministing constant reload on Safari if the if statement is removed
    if (e.originalEvent.state !== null) {
        window.location.href = location.href;
    }
})


Product.Config.prototype.getIdOfSelectedProduct = function()
{
    var existingProducts = new Object();

    for(var i=this.settings.length-1;i>=0;i--)
    {
        var selected = this.settings[i].options[this.settings[i].selectedIndex];
        if(selected && selected.config)
        {
            for(var iproducts=0;iproducts<selected.config.allProducts.length;iproducts++)
            {
                var usedAsKey = selected.config.allProducts[iproducts]+"";
                if(existingProducts[usedAsKey]==undefined)
                {
                    existingProducts[usedAsKey]=1;
                }
                else
                {
                    existingProducts[usedAsKey]=existingProducts[usedAsKey]+1;
                }
            }
        }
    }

    for (var keyValue in existingProducts)
    {
        for ( var keyValueInner in existingProducts)
        {
            if(Number(existingProducts[keyValueInner])<Number(existingProducts[keyValue]))
            {
                delete existingProducts[keyValueInner];
            }
        }
    }

    var sizeOfExistingProducts=0;
    var currentSimpleProductId = "";
    for ( var keyValue in existingProducts)
    {
        currentSimpleProductId = keyValue;
        sizeOfExistingProducts=sizeOfExistingProducts+1
    }

    if(sizeOfExistingProducts==1)
    {
        return currentSimpleProductId;
    }
}
