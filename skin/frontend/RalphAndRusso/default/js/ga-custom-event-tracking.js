jQuery(document).ready(function($) {
    // PLP Buy Button
    $(document).on('click', ".view-all-item .wrapper a", function(e) {
        var CatValue, ActionValue, LabelValue;
    
        if ($(this).hasClass("btn")) {
            CatValue = "PLP Product Button Clicked";
            ActionValue = $(this).parent().parent().parent(".wrapper").attr("data-product-sku");
            LabelValue = window.location.pathname;
        } else {
            CatValue = "PLP Product Image Clicked";
            ActionValue = $(this).parent(".wrapper").attr("data-product-sku");
            LabelValue = window.location.pathname;
        }
    
        GASend(CatValue, ActionValue, LabelValue);
    });

    // PDP Add To Bag
    $(document).on('click', "#product-addtocart-button", function (e) {
        var CatValue, ActionValue, LabelValue;

        if ($("#configurable_swatch_shoe_size").length){
            CatValue = "PDP Add To Bag Swatches";
            ActionValue = $("#product-single-header").attr("data-product-sku");
            LabelValue = $("#configurable_swatch_shoe_size .selected a span").text();
        }  else {
            if ($("#product-options-wrapper select:first").hasClass("no-display")) {
                CatValue = "PDP Add To Bag One Size";
                ActionValue = $("#product-single-header").attr("data-product-sku");
                LabelValue = "Bag Size";
            } else {
                CatValue = "PDP Add To Bag Dropdown";
                ActionValue = $("#product-single-header").attr("data-product-sku");
                LabelValue = $("#product-options-wrapper select:first option:selected").text();
            }
        }
        
        GASend(CatValue, ActionValue, LabelValue);
    });

    // Cart Checkout Button
    $(document).on('click', ".minicart .checkout-button", function(e) {
        var CatValue, ActionValue, LabelValue;
        
        var product_list;

        $("#cart-sidebar li").each(function(el) {
            var product_name, product_quantity, product_price;

            product_name = $(this).attr("data-product-sku");
            product_quantity = $(this).children(".product-details").children(".truncated").children(".truncated_full_value").children(".item-options").children("div").children("dd").children("strong").text();
            product_price = $(this).children(".product-details").children(".truncated").children(".price").text();

            if (product_list == undefined) {
                product_list = product_name + ", " + product_quantity + ", " + product_price;
            } else {
                product_list += " | " + product_name + ", " + product_quantity + ", " + product_price;
            }
            
        })

        CatValue = "Cart Checkout Clicked";
        ActionValue = product_list;
        LabelValue = window.location.pathname + ", " + $("#cost-summary-total .price").text();

        GASend(CatValue, ActionValue, LabelValue);
    });

    // Cart Remove Product
    $(document).on('click', '.remove a', function(e){
        var CatValue, ActionValue, LabelValue;

        CatValue = "Cart Remove Product";
        ActionValue = $(this).parent().parent().parent().attr("data-product-sku") + ", " + $(this).parent().parent().next(".product-details").children(".truncated").children(".truncated_full_value").children(".item-options").children("div").children("dd").children("strong").text();
        LabelValue = window.location.pathname;

        GASend(CatValue, ActionValue, LabelValue);
    });

    // Top Level Nav Clicked
    $(document).on('click', '.menu', function(e){
        var CatValue, ActionValue, LabelValue;

        if ($(this).hasClass("inline-name")) {
            
        } else if ($(this).children("div").hasClass("name")){
            
        } else {
            CatValue = "Top Level Nav Clicked";
            ActionValue = $(this).children().children().children().text();
            LabelValue = window.location.pathname;
        }

        GASend(CatValue, ActionValue, LabelValue);
    });

    // Sub Level List Nav Clicked
    $(document).on('click', '.inline-name', function(e){
        var CatValue, ActionValue, LabelValue;
    
        var topCatNumber = $(this).parents(".wp-custom-menu-popup").attr("id").replace("popup", "");

        CatValue = "Sub Level List Nav Clicked";
        ActionValue = $("#menu" + topCatNumber).children().children().text() + " > "+ $(this).children(".name").children("span").text();
        LabelValue = window.location.pathname;

        GASend(CatValue, ActionValue, LabelValue);
    });

    // Sub Level Image Nav Clicked
    $(document).on('click', '.itemMenuName', function(e){
        var CatValue, ActionValue, LabelValue;

        var topCatNumber = $(this).parents(".wp-custom-menu-popup").attr("id").replace("popup", "");

        CatValue = "Sub Level Image Nav Clicked";
        ActionValue = $("#menu" + topCatNumber).children().children().text() + " > "+ $(this).children("a").children(".name").children("span").text();
        LabelValue = window.location.pathname;

        GASend(CatValue, ActionValue, LabelValue);
    });

    // Contact Us Form Button Click
    $(document).on('click', '#contactForm .btn', function(e){
        var CatValue, ActionValue, LabelValue, QACheck;
        
        CatValue = "Contact Us Form";
        ActionValue = $("#contact-email").val();
        LabelValue = $("#contact-message").val();
        QACheck = true;

        $('#contactForm *').filter(':input').each(function(){
            if ($(this).attr("type") != "submit" && $(this).attr("id") != "hideit") {
                if ($(this).val() == "") {
                    QACheck = false;
                }
            }
        });
        
        if (QACheck === true) {
            GASend(CatValue, ActionValue, LabelValue);
        }   
    });

    // Look Book Form Button Click
    $(document).on('click', '#bookAnEnquiryForm .button', function(e){
        var CatValue, ActionValue, LabelValue, QACheck;
        
        CatValue = "Look Book Form";
        ActionValue = $("#enquiry-email").val();
        LabelValue = $("#enquiry-message").val();
        QACheck = true;

        $('#bookAnEnquiryForm *').filter(':input').each(function(){
            if ($(this).attr("type") != "submit" && $(this).attr("id") != "hideit") {
                if ($(this).val() == "") {
                    QACheck = false;
                }
            }
        });
        
        if (QACheck === true) {
            GASend(CatValue, ActionValue, LabelValue);
        }   
    });

    // Appiontment Form Button Click
    $(document).on('click', '#bookAnAppointmentForm .button', function(e){
        var CatValue, ActionValue, LabelValue, QACheck;
        
        CatValue = "Appiontment Form";
        ActionValue = $("#appointment-email").val();
        LabelValue = $("#appointment-message").val();
        QACheck = true;

        $('#bookAnAppointmentForm *').filter(':input').each(function(){
            if ($(this).attr("type") != "submit" && $(this).attr("id") != "hideit") {
                if ($(this).val() == "") {
                    QACheck = false;
                }
            }
        });
        
        if (QACheck === true) {
            GASend(CatValue, ActionValue, LabelValue);
        }   
    });

    function GASend(CatValue, ActionValue, LabelValue) {
        if (window.location.hostname === "127.0.0.1") {
            alert("event | " + CatValue + " | " + ActionValue + " | " + LabelValue);
        } else {
            ga('send', {
                hitType: 'event',
                eventCategory: CatValue,
                eventAction: ActionValue,
                eventLabel: LabelValue
            });
        }
    }
});