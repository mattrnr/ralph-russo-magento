if (typeof jQuery === "undefined") { throw new Error("Behaviors requires jQuery") }

(function (jQuery) {
    /**
     * Call this method passing the behavior name to call the behavior
     *
     * Additional options can be passed to the function, e.g jQuery("#main").behavior("sticky-footer", "#option1", "#option2")
     * To learn which options can be passed, please consult documentation for specific behaviors.
     * @name  behavior
     * @param {string} behaviorName - The name of the behavior.
     */
     jQuery.fn.behavior = function(behaviorName) {
      return qi.behaviors[behaviorName].apply( this, Array.prototype.slice.call( arguments, 1 ));
    }

    /**
     * Registers new behavior
     *
     * @name addBehavior
     * @param {string} behaviorName - The name of the behavior. Needs to be unique
     * @param {string} fnc - The name of the behavior.
     */
     jQuery.fn.addBehavior = function(behaviorName, fnc) {

      try {
        qi.behaviors;
      } catch (e) {
        qi = window.qi || {};
        qi.behaviors = qi.behaviors || [];
      }

      if (qi.behaviors[behaviorName] != null)
        throw "Behavior with the name: " + behaviorName + " has already been registered."

      qi.behaviors[behaviorName] = fnc;
    };
  }(jQuery));

  jQuery.fn.addBehavior("equalise-height", function() {

    var maxHeight = 0;
    jQuery(this).each(function(i, el) {

     if (jQuery(el).height() > maxHeight)
      maxHeight = jQuery(el).height()

  })

    if (maxHeight)
     jQuery(this).height(maxHeight)

   return this;
 });

  ;jQuery.fn.addBehavior("grid", function() {
    this.addClass("b-grid");
    this.find(" > *").addClass('b-grid-cell');

    this.find(" > *").each(function(i, el) {
     if (jQuery(el).inlineStyle("width") != "")
      jQuery(el).css("flex", "none")
  });

    return this;
  });

  jQuery.fn.addBehavior("grid-gutters", function() {
    this.css({
     "margin-left" : -parseInt(this.find("> *").first().css("margin-left"))
   })

    return this;
  });

/**
 * Parse DOM and apply behavior
 */
 jQuery(window).ready(function() {
  jQuery(".grid").behavior("grid");
  jQuery(".grid-gutters").behavior("grid-gutters")
})


/**
 * TODO update this method to also check if property set in CSS. Will remove the need for grid-defined-width property
 * Helper function which checks if a property has been set by the user (ONLY inline)
 */
 ;(function (jQuery) {
  jQuery.fn.inlineStyle = function (prop) {
    return this.prop("style")[jQuery.camelCase(prop)];
  };
}(jQuery));;jQuery.fn.addBehavior("horizontal-centering-with-floated-left-elements", function(containerMargin, options) {

  var defaultOptions = {

    windowWidthToDisplayInline: 767,
  };

  options = jQuery.extend(defaultOptions, options);

  var that = this;

  that.parent().addClass("horizontal-flex-grid-container");
  itemWidth = that.width();
  itemWidth += containerMargin;
  var main = jQuery("#main");

  var fitItems = Math.floor(main.width() / itemWidth);

  that.parent().width(fitItems * itemWidth);

  jQuery(window).width() <= options.windowWidthToDisplayInline ? that.parent().addClass('change-layout-mode') : that.parent().removeClass('change-layout-mode');
});

/**
 * Parse DOM and apply behavior
 */
 jQuery(window).ready(function() {

  jQuery(window).on('resize', function() {
    jQuery(".box").behavior("horizontal-centering-with-floated-left-elements", 15);

  }).trigger('resize');

});


 ;jQuery.fn.addBehavior("scalable-element", function(options) {


  var defaultOptions = {


    relativeToElement: null,


    relativeToWidth: null
  };

  options = jQuery.extend(defaultOptions, options);

  var relativeToElement = options.relativeToElement || jQuery(this).parent();
  var relativeToWidth = parseInt(options.relativeToWidth) || jQuery(relativeToElement).width();

  this.each(function(){

    var isImg = (this.nodeName.toLowerCase() === 'img');
    var el = jQuery(this);

    if(isImg) {
      var img = new Image();
      img.src = jQuery(this).attr('src');
      img.onload = function() {
        if(img.width > relativeToWidth)
         el.css({width: '100%'});
       else {
         var elementWidthPercentage = (img.width/relativeToWidth)*100;
         el.css({width: elementWidthPercentage + "%"});
       }
     }
   }
   else {
    if(jQuery(this).width() > relativeToWidth)
      jQuery(this).css({width: '100%'});
    else {
      var elementWidthPercentage = (jQuery(this).width()/relativeToWidth)*100;
      jQuery(this).css({width: elementWidthPercentage + "%"});
    }
  }
});

  return this;
});

/**
 * Parse DOM and apply behavior
 */
 jQuery(window).ready(function() {
  jQuery(".scalable-element").behavior("scalable-element");
});

// jQuery(window).on('resize', function() {
//  jQuery(".scalable-element").behavior("scalable-element");
// });

  ;jQuery.fn.addBehavior("scroll-to-top", function() {

    this.each(function() {
     jQuery(this).on('click', function() {
      jQuery("html, body").stop().animate({ scrollTop: 0 }, 'slow');
    });
   });
    return this;
  });

/**
 * Parse DOM and apply behavior
 */
 jQuery(window).ready(function() {
  jQuery(".scroll-to-top").behavior("scroll-to-top");
});

 ;

 jQuery.fn.addBehavior("sticky-footer", function(contentContainer) {

     jQuery(window).resize(function() {

       if(jQuery(window).height() > (jQuery('header, #header').outerHeight(true) + jQuery('#mobile-nav').outerHeight(true) + jQuery('#footer, footer, .footer-container').outerHeight(true) + jQuery('#main').outerHeight(true))) {
         jQuery('#footer').css({
           position: 'fixed',
           bottom: '0px',
           opacity: 1,
           width: jQuery('#main').width()
         })

       } else {
         jQuery('#footer').css({
           position: 'relative',
           opacity: 1
         })

       }

     }).trigger('resize');

     return this;

});

/**
 * Parse DOM and apply behavior
 */
 jQuery(window).ready(function() {
  //jQuery(".sticky-footer").behavior("sticky-footer");
});(function() {

  jQuery.fn.addBehavior("vertical-centering", function() {

   if (Modernizr.flexbox) {
    this.each(function(i) {

      var position = jQuery(this).css("position");

      if (position == "fixed" || position == "absolute") {
        jQuery(this).css({
          top: "50%",
          "margin-top": -(jQuery(this).height()/2)
        })
      } else {
        jQuery(this).parent().addClass("vertical-flex-grid-container");
      }
    });
  } else {
    polyfil(this);
  }

  return this;
});

  function polyfil(el) {

   el.each(function(i) {
    that = jQuery(this);
    that.css({
     "display" : "inline-block",
     "vertical-align" : "middle",
     "position" : "relative",
     "top" : that.parent().height() / 2 - (that.outerHeight() / 2)
   });
  });
 }

   /**
    * Parse DOM and apply behavior
    */
    jQuery(window).ready(function() {
      jQuery(".vertical-centering").behavior("vertical-centering");
    });

  })();
