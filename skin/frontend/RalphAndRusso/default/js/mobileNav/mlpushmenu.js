jQuery(window).ready(function() {

	jQuery(window).resize(function() {
		if(jQuery(window).width() <= 767) {
			jQuery('#mobile-menu-trigger').on('click', function() {
				jQuery(".menu-mobile a.open").trigger("click");
				jQuery('body').addClass('mobile-nav-active');
				ralphAndRusso.showImage("#mp-menu img[data-src]");
			});
		} else {
			jQuery('body').removeClass('mobile-nav-active');
		}
	}).trigger('resize');


	jQuery('#newsletter-modal').on({
		'show.uk.modal': function(){
			jQuery('#menu-close-btn').trigger('click');
		}
	});



	jQuery('#menu-close-btn').on('click', function() {
		jQuery('body').removeClass('mobile-nav-active');
	})
});
