jQuery(window).ready(function($) {

	if(!jQuery('.multiple-items-carousel').hasClass('slick-initialized')) {
		$('.multiple-items-carousel').slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			centerPadding: '30px',
			responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			]
		});
	}

	$('.slick-slide .hover-container a').behavior("vertical-centering");
});
